<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'HomeController@index');
//Client Routes
Route::get('/client/search', 'ClientsController@search');//Json
Route::get('/client', 'ClientsController@index');
Route::get('/client/create', 'ClientsController@create');
Route::get('/client/{client}', 'ClientsController@show');
Route::post('/client', 'ClientsController@store');
Route::get('/client/{id}/edit', 'ClientsController@edit');
Route::put('/client/{id}', ['as' => 'client.update','uses' => 'ClientsController@update']);
Route::delete('/client/delete',['as' => 'client.delete','uses' => 'ClientsController@destroy']);

//Control Template Routes
Route::get('/control-template/get', 'ControlTemplatesController@get');//Json
Route::get('/control-template', 'ControlTemplatesController@index');
Route::get('/control-template/create', 'ControlTemplatesController@create');
Route::post('/control-template', 'ControlTemplatesController@store');
Route::delete('/control-template/delete',['as' => 'control-template.delete','uses' => 'ControlTemplatesController@destroy']);
Route::get('/control-template/{template}', 'ControlTemplatesController@show');

//Person Routes
Route::get('/person/search', 'PersonsController@search');//Json
Route::get('/person', 'PersonsController@index');
Route::get('/person/create', 'PersonsController@create');
Route::post('/person', 'PersonsController@store');
Route::get('/person/{rut}/edit', 'PersonsController@edit');
Route::put('/person/{rut}', ['as' => 'person.update','uses' => 'PersonsController@update']);
Route::delete('/person/delete',['as' => 'person.delete','uses' => 'PersonsController@destroy']);

//Report Generator Routes
Route::get('/report-generator', 'ReportGeneratorController@index');
Route::get('/report-generator/two', 'ReportGeneratorController@step_two');
Route::get('report-generator/getGraphicData', 'ReportGeneratorController@getGraphicData');//Json

//Scheduled Visits Routes
Route::get('/scheduled-visit/search', 'ScheduledVisitsController@search');//Json
Route::post('/scheduled-visit/dates', 'ScheduledVisitsController@datesCalendar');//Json
Route::get('/scheduled-visit', 'ScheduledVisitsController@index');
Route::get('/scheduled-visit/calendar', 'ScheduledVisitsController@calendar');
Route::get('/scheduled-visit/create', 'ScheduledVisitsController@create');
Route::post('/scheduled-visit', 'ScheduledVisitsController@store');
Route::get('/scheduled-visit/{id}/edit', 'ScheduledVisitsController@edit');
Route::put('/scheduled-visit/{id}', ['as' => 'scheduled-visit.update','uses' => 'ScheduledVisitsController@update']);
Route::delete('/scheduled-visit/delete',['as' => 'scheduled-visit.delete','uses' => 'ScheduledVisitsController@destroy']);
Route::get('/scheduled-visit/{scheduledVisit}', 'ScheduledVisitsController@show');

//Trap Type Routes
Route::get('/trap-type/search', 'TrapTypesController@search');//Json
//Users Routes
Route::get('/user', 'UsersController@index');
Route::get('/user/create', 'UsersController@create');
Route::post('/user', 'UsersController@store');
Route::get('/user/{id}/edit', 'UsersController@edit');
Route::put('/user/{id}', ['as' => 'user.update', 'uses' => 'UsersController@update']);
Route::delete('/user/delete',['as' => 'user.delete','uses' => 'UsersController@destroy']);
Route::get('/user/{rut}', 'UsersController@show');

//Visits-Users Routes
Route::get('/visits-users/search', 'VisitsUsersController@search');//Json
Route::get('/visits-users/get-free', 'VisitsUsersController@getFree');//Json
Route::post('/visits-users','VisitsUsersController@store');
