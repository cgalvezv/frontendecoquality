<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id_visit')->unsigned();          
            $table->string('rut_user');       
            $table->primary(['id_visit', 'rut_user']);    
            $table->timestamps();
        });

        Schema::table('visits_users', function($table) {
            $table->foreign('id_visit')->references('id')->on('scheduled_visits')->onDelete('cascade');
            $table->foreign('rut_user')->references('rut_person')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits_users');
    }
}
