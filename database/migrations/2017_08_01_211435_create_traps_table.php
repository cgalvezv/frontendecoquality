<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traps', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_client')->unsigned();
            $table->integer('id_trap_type')->unsigned(); 
            $table->integer('id_trap_location')->unsigned(); 
            $table->integer('trap_number');
            $table->char('trap_letter', 1)->nullable()->default(null); 
            $table->timestamps();
        });

        Schema::table('traps', function($table) {
            $table->foreign('id_client')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('id_trap_type')->references('id')->on('trap_types')->onDelete('cascade');
            $table->foreign('id_trap_location')->references('id')->on('trap_locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traps');
    }
}
