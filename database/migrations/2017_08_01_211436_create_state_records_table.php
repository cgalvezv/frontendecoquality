<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_records', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_trap');
            $table->datetime('register_date');                                
            $table->integer('id_trap_state')->unsigned();                       
            $table->boolean('is_registered')->default(false);
            $table->timestamps();
        });

        Schema::table('state_records', function($table) {            
            $table->foreign('id_trap_state')->references('id')->on('trap_states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state_records');
    }
}
