<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_client')->unsigned();            
            $table->integer('id_control_template_type')->unsigned();            
            $table->integer('total_trap');
            $table->timestamps();
        });

        Schema::table('control_templates', function($table) {
            $table->foreign('id_client')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('id_control_template_type')->references('id')->on('control_template_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_templates');
    }
}