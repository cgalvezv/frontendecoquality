<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduledVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_visits', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_control_template')->unsigned();            
            $table->datetime('visit_date');
            $table->text('findings')->nullable();
            $table->text('actions')->nullable();
            $table->boolean('isVisited')->default(false);
            $table->timestamps();
        });

        Schema::table('scheduled_visits', function($table) {
            $table->foreign('id_control_template')->references('id')->on('control_templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_visits');
    }
}