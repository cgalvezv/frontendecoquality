<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduledVisit extends Model
{
    protected $fillable = ['id_control_template', 'visit_date'];
}
