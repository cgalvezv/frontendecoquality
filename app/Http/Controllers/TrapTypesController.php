<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrapTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function search(Request $request)
    {
        $term = $request->term;

        $types = \DB::table('trap_types')                    
                    ->select('trap_types.id','trap_types.name')
                    ->whereRaw('(trap_types.name LIKE "%'.$term.'%")')
                    ->get();  

        $response = array();

        foreach ($types as $type) 
        {
        	$object = array(
        		'id' => $type->id,
            	'name' => ucfirst($type->name)
        	);            
            array_push($response, $object);         
        }
        
        return \Response::json($response);
    }
}
