<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Client;
use App\ControlTemplate;
use App\Trap;
use App\TrapType;
use App\TrapState;
use App\StateRecord;
use App\ScheduledVisit;

class ReportGeneratorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	return view('report-generator.index');
    }

    public function step_two(Request $request)
    {
        $client = $request->client_id;
        $trapType = $request->trap_type_id;
        $period = $request->period;        

        $lastTemplate = ControlTemplate::where('id_client', '=', $client)
                        ->where('id_control_template_type', '=', $trapType)
                        ->first();
        
        $visits = ScheduledVisit::where('id_control_template', '=', $lastTemplate->id)
                    ->orderBy('visit_date', 'desc')
                    ->take($period)
                    ->get();

        if(sizeof($visits) == 0)
        {
            dd("No existen visitas");
        }

        $lastVisit =  $visits[0];        

        $traps = Trap::where('id_client', '=', $client)
                        ->where('id_trap_type', '=', $trapType)
                        ->get();

        $dataStepOne = array(
                'client_id' => $client,
                'trap_type_id' => $trapType,
                'period' => $period                
        );

        $count1 = 0; $count2 = 0; $count3 = 0; $count4 = 0; $count5 = 0; 

        foreach ($traps as $trap) {
            $state = StateRecord::where('id_trap', '=', $trap->id)
                                ->where('register_date', '=', $lastVisit->visit_date)
                                ->first();

            if ($state->id_trap_state == 1 or $state->id_trap_state == 6) $count1++;
            if ($state->id_trap_state == 2 or $state->id_trap_state == 7) $count2++;
            if ($state->id_trap_state == 3 or $state->id_trap_state == 8) $count3++;
            if ($state->id_trap_state == 4 or $state->id_trap_state == 9) $count4++;
            if ($state->id_trap_state == 5) $count5++;
        }

        $countStates = array($count1,$count2,$count3,$count4,$count5);

        $historicPercentage = $this->calculateHistoricPercentage($visits, $traps);

        $dataTable = array();
        $dataIntensity = array();

        if ($trapType == 1) {
            $trapStates = TrapState::where('id', '>=', '1')
                                ->where('id', '<=', '5')
                                ->get();
        }
        else {
            $trapStates = TrapState::where('id', '>=', '6')
                                ->where('id', '<=', '9')
                                ->get();
        }

        $intensity = 0;
        $intensityHistoric = 0;
        $index = 0;
        foreach ($trapStates as $state) 
        {
            $object = array(
                        'abbr' => $state->abbreviation,
                        'name' => $state->name,
                        'count' => $countStates[$index],
                        'percentage' => round(($countStates[$index] * 100)/count($traps), 2),
                        'historicPercentage' => $historicPercentage[$index]
            );

            if ($trapType == 1) 
            {
                if($index == 0 or $index == 1)
                {
                    $intensity += round(($countStates[$index] * 100)/count($traps), 2);
                    $intensityHistoric += $historicPercentage[$index];
                }
            }
            else
            {
                if($index == 1)
                {
                    $intensity += round(($countStates[$index] * 100)/count($traps), 2);
                    $intensityHistoric += $historicPercentage[$index];
                }
            }
            array_push($dataTable, $object);  
            $index++;
        }

        if ($intensity > 80 ) {
            array_push($dataIntensity, "ALTA");  
        } else {
            if ($intensity >= 40 and $intensity <= 80) 
            {
                array_push($dataIntensity, "MEDIA"); 
            } else {
                array_push($dataIntensity, "BAJA"); 
            }        
        }

        if ($intensityHistoric > 80 ) {
            array_push($dataIntensity, "ALTA");  
        } else {
            if ($intensityHistoric >= 40 and $intensityHistoric <= 80) 
            {
                array_push($dataIntensity, "MEDIA"); 
            } else {
                array_push($dataIntensity, "BAJA"); 
            }        
        }
        

    	return view('report-generator.step_two')->with(compact('dataStepOne', 'dataTable', 'dataIntensity'));
    }

    //JSON Request's
    public function getGraphicData(Request $request)
    {
        
        $client = $request->client_id;
        $trapType = $request->trap_type_id;
        $period = $request->period;

        $colors = array(
            array(
                'borderColor' => 'rgba(18, 151, 147, 1)',
                'backgroundColor' => 'rgba(18, 151, 147, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(172, 62, 62, 1)',
                'backgroundColor' => 'rgba(172, 62, 62, 0.3)',
            ),
            array(
                'borderColor' => 'rgba(90, 80, 80, 1)',
                'backgroundColor' => 'rgba(90, 80, 80, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(130, 84, 120, 1)',
                'backgroundColor' => 'rgba(130, 84, 120, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(18, 151, 59, 1)',
                'backgroundColor' => 'rgba(18, 151, 59, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(18, 47, 151, 1)',
                'backgroundColor' => 'rgba(18, 47, 151, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(151, 18, 110, 1)',
                'backgroundColor' => 'rgba(151, 18, 110, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(179, 174, 26, 1)',
                'backgroundColor' => 'rgba(179, 174, 26, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(208, 122, 34, 1)',
                'backgroundColor' => 'rgba(208, 122, 34, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(242, 213, 63, 1)',
                'backgroundColor' => 'rgba(242, 213, 63, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(147, 214, 14, 1)',
                'backgroundColor' => 'rgba(147, 214, 14, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(16, 164, 238, 1)',
                'backgroundColor' => 'rgba(16, 164, 238, 0.3)'
            ),
            array(
                'borderColor' => 'rgba(239, 112, 248, 1)',
                'backgroundColor' => 'rgba(239, 112, 248, 0.3)'
            )
        );

        //dd($colors[0]['borderColor']);

        $lastTemplate = ControlTemplate::where('id_client', '=', $client)
                        ->where('id_control_template_type', '=', $trapType)
                        ->first();
        
        $visits = ScheduledVisit::where('id_control_template', '=', $lastTemplate->id)
                    ->orderBy('visit_date', 'desc')
                    ->take($period)
                    ->get();

        $lastVisit =  $visits[0];        

        $traps = Trap::where('id_client', '=', $client)
                        ->where('id_trap_type', '=', $trapType)
                        ->get();

        if ($trapType == 1) {
            $trapStates = TrapState::where('id', '>=', '1')
                                ->where('id', '<=', '5')
                                ->get();
        }
        else {
            $trapStates = TrapState::where('id', '>=', '6')
                                ->where('id', '<=', '9')
                                ->get();
        }

        $labels = array();

        foreach ($visits as $visit) 
        {
            array_push($labels, Carbon::parse($visit->visit_date)->format('M-d'));             
        }

        $dataSets = array();

        $index = 0;
        foreach ($trapStates as $trapState) 
        {
            $data = array();
            $historicStates = array();            

            foreach ($visits as $visit) 
            {
                $count1 = 0; $count2 = 0; $count3 = 0; $count4 = 0; $count5 = 0; 

                foreach ($traps as $trap) {
                    $state = StateRecord::where('id_trap', '=', $trap->id)
                                        ->where('register_date', '=', $visit->visit_date)
                                        ->first();

                    if ($state->id_trap_state == 1 or $state->id_trap_state == 6) $count1++;
                    if ($state->id_trap_state == 2 or $state->id_trap_state == 7) $count2++;
                    if ($state->id_trap_state == 3 or $state->id_trap_state == 8) $count3++;
                    if ($state->id_trap_state == 4 or $state->id_trap_state == 9) $count4++;
                    if ($state->id_trap_state == 5) $count5++;
                }

                $countStates = array($count1,$count2,$count3,$count4,$count5);
                array_push($historicStates, $countStates); 
            }

            $historicStates = array_reverse($historicStates);

            for ($i = 0; $i < count($historicStates); $i++)
            {
                $percentage = round(($historicStates[$i][$index] * 100)/count($traps),2);
                array_push($data, $percentage); 
            }

            $dataSet = array(
                'label' => $trapState->name,
                'backgroundColor' => $colors[$index]['backgroundColor'],
                'borderColor' => $colors[$index]['borderColor'],
                'data' => $data,
                'fill' => false,
                'borderWidth' => 2
            );
            array_push($dataSets, $dataSet); 

            $index++;
        }

        $response = array(
            'labels' => array_reverse($labels),
            'dataSets' => $dataSets
        );

        return \Response::json($response);
    }

    //Private Functions
    private function calculateHistoricPercentage($visits, $traps)
    {
        $historicStates = array();
        $historicPercentage = array();

        foreach ($visits as $visit) 
        {
            $count1 = 0; $count2 = 0; $count3 = 0; $count4 = 0; $count5 = 0; 

            foreach ($traps as $trap) {
                $state = StateRecord::where('id_trap', '=', $trap->id)
                                    ->where('register_date', '=', $visit->visit_date)
                                    ->first();

                if ($state->id_trap_state == 1 or $state->id_trap_state == 6) $count1++;
                if ($state->id_trap_state == 2 or $state->id_trap_state == 7) $count2++;
                if ($state->id_trap_state == 3 or $state->id_trap_state == 8) $count3++;
                if ($state->id_trap_state == 4 or $state->id_trap_state == 9) $count4++;
                if ($state->id_trap_state == 5) $count5++;
            }

            $countStates = array($count1,$count2,$count3,$count4,$count5);
            array_push($historicStates, $countStates); 
        }

        for ($j = 0; $j < count($historicStates[0]); $j++)
        {
            $sum = 0;
            for ($i = 0; $i < count($historicStates); $i++)
            {
                $sum += $historicStates[$i][$j];
            }
            $count = $sum/count($visits);
            $percentage = ($count * 100)/count($traps);
            array_push($historicPercentage, round($percentage, 2));
        }

        return $historicPercentage;        

    }

}
