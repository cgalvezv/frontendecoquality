<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\VisitsUser;

class VisitsUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function search(Request $request)
    {
        $term = $request->term;

        $persons = \DB::table('persons') 
                    ->join('users', 'persons.rut', '=', 'users.rut_person')                   
                    ->select('persons.rut','persons.name','persons.lastname')
                    ->where('users.id_user_role', '=', 3)
                    ->whereRaw('(persons.name LIKE "%'.$term.'%" or persons.lastname LIKE "%'.$term.'%")')
                    ->get();  
        
        
        $response = array();

        foreach ($persons as $person) 
        {
            $object = array( 
                'rut' => $person->rut,
                'name' => ucfirst($person->name).' '.ucfirst($person->lastname)
            );
            array_push($response, $object);         
        }

        return \Response::json($response);
    }

    public function getFree(Request $request)
    {
        $idVisitsUsers = $request->id_visit;
        $templatesUser = VisitsUser::where('id_visit', '=', $idVisitsUsers)->get();

        $responseVisitsUser = array();

        if (count($templatesUser) > 0) {
            foreach ($templatesUser as $VisitsUser) {
                $object = persons::where('rut', '=', $VisitsUser->rut_user)->get();
                array_push($responseVisitsUser, $object);
            }
        }
        
        return \Response::json($responseVisitsUser);
    }
}
