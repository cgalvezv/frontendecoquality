<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Client;

class ClientsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$clients = Client::all();    	
    	return view('client.index')->with(compact('clients'));
    }

    public function show(Client $client)
    {
        return view('client.show')->with(compact('client'));
    }

    public function create()
    {
        return view('client.create');
    }

    public function store(Request $request)
    { 
        if ($request->hasFile('url_plan_location')) {
            $client = Client::create([
                'name' => $request->name,
                'email' => $request->email,
                'physical_place' => $request->physical_place,
                'url_plan_location' => ''
            ]);            
            $path = 'storage/img/client/'.$client->id;
            Storage::makeDirectory($path);
            $imageExtension = $request->file('url_plan_location')->clientExtension();            
            $filename = 'client-'.$client->id.'.'.$imageExtension;
            $request->url_plan_location->storeAs($path, $filename);
            $client->url_plan_location = $path.'/'.$filename;  
            $client->save();          
        }        
        return redirect('/client');
    }

    public function edit($id)
    {
        $client = Client::find($id);
        return view('client.edit')->with(compact('client')); 
    }

    public function update($id, Request $request)
    {
        $client = Client::find($id);
        $client->name = $request->name;
        $client->email = $request->email;
        $client->physical_place = $request->physical_place;
        if ($request->hasFile('url_plan_location')) {
            $path = 'storage/img/client/'.$id;        
            Storage::deleteDirectory($path);
            Storage::makeDirectory($path);
            $imageExtension = $request->file('url_plan_location')->clientExtension();            
            $filename = 'client-'.$id.'.'.$imageExtension;
            $request->url_plan_location->storeAs($path, $filename);
            $client->url_plan_location = $path.'/'.$filename;  
        }
        $client->save();
        return redirect('/client');
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $path = 'public/img/client/'.$id;        
        Storage::deleteDirectory($path);
        Client::where('id', '=', $id)->delete();
        return redirect('/client');
    }    

    //JSON Request's
    public function search(Request $request)
    {
        $term = $request->term;

        $clients = \DB::table('clients')                    
                    ->select('clients.id','clients.name','clients.physical_place')
                    ->whereRaw('(clients.name LIKE "%'.$term.'%" or clients.physical_place LIKE "%'.$term.'%")')
                    ->get();  

        $response = array();

        foreach ($clients as $client) 
        {
        	$object = array(
        		'id' => $client->id,
            	'full_name' => ucfirst($client->name).' - '.ucfirst($client->physical_place)
        	);    
       
            array_push($response, $object);         
        }        
        return \Response::json($response);
    }
}
