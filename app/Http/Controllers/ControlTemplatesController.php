<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use App\ControlTemplate;
use App\ControlTemplateType;
use App\Trap;

class ControlTemplatesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index()
	{
		$templates = ControlTemplate::all();
		$data = array();

		foreach ($templates as $template) {
			$client = Client::find($template->id_client);
			$type = ControlTemplateType::find($template->id_control_template_type);

			$object = array( 
    			'id' => $template->id,
    			'control_template_type' => $type->name,
    			'client_name' => $client->name,
    			'client_physical_place' => $client->physical_place,    			
    			'total_trap' => $template->total_trap
    		);
    		array_push($data, $object); 
		}

		return view('control-template.index')->with(compact('data'));
	}

    public function show(ControlTemplate $template)
    {        
        $traps = Trap::where('id_client', $template->id_client)
                    ->where('id_trap_type', $template->id_control_template_type)
                    ->get();
        $north_traps = array();$south_traps = array();$east_traps = array();$west_traps = array();

        foreach ($traps as $trap) 
        {
            if ($trap->id_trap_location == 1){                
                if ($trap->id_trap_type == 1){
                    array_push($north_traps, $trap->trap_number);
                } else {
                    if ($trap->trap_number == 1) {
                        array_push($north_traps, $trap->trap_letter);
                    } else {
                        array_push($north_traps, $trap->trap_letter.''.$trap->trap_number);
                    }
                    
                }
            }
            if ($trap->id_trap_location == 2){                
                if ($trap->id_trap_type == 1){
                    array_push($south_traps, $trap->trap_number);
                } else {
                    if ($trap->trap_number == 1) {
                        array_push($south_traps, $trap->trap_letter);
                    } else {
                        array_push($south_traps, $trap->trap_letter.''.$trap->trap_number);
                    }
                    
                }
            }
            if ($trap->id_trap_location == 3){                
                if ($trap->id_trap_type == 1){
                    array_push($east_traps, $trap->trap_number);
                } else {
                    if ($trap->trap_number == 1) {
                        array_push($east_traps, $trap->trap_letter);
                    } else {
                        array_push($east_traps, $trap->trap_letter.''.$trap->trap_number);
                    }
                    
                }
            }
            if ($trap->id_trap_location == 4){                
                if ($trap->id_trap_type == 1){
                    array_push($west_traps, $trap->trap_number);
                } else {
                    if ($trap->trap_number == 1) {
                        array_push($west_traps, $trap->trap_letter);
                    } else {
                        array_push($west_traps, $trap->trap_letter.''.$trap->trap_number);
                    }
                    
                }
            }
            
        }
        return view('control-template.show')->with(compact(['template','north_traps','south_traps','east_traps','west_traps']));
    }

	public function create()
    {
        $controlTemplateType = ControlTemplateType::all();
        return view('control-template.create')->with(compact('controlTemplateType'));
    }

    public function store(Request $request)
    {    	
    	$totalTrap = $request->total_trap;
    	$templateType = $request->template_id;
    	$client = $request->client_id;
    	$trap_locations = array(
    		$request->north_values, 
    		$request->south_values,
    		$request->east_values,
    		$request->west_values
    	);

    	$saved_traps = array();
        $inArray_traps = array();
    	$countLocation = 1;

    	foreach ($trap_locations as $traps) 
    	{
    		$firstTraps = explode(",", $traps);
    		foreach ($firstTraps as $firstTrap) 
    		{

    			$secondTraps = explode("-", $firstTrap);                 
    			if (sizeof($secondTraps) > 1) 
    			{
    				$init = intval($secondTraps[0]);
    				$end = $secondTraps[1];
    				for ($i = $init; $i <= $end ; $i++) 
    				{                            				
    					if (in_array($i, $inArray_traps, true) or ($i < 1 or $i > $totalTrap)) {
	    					dd("Repetido o excede el maximo");
	    				} else {
	    					if ($templateType == 1) {
	    						array_push($saved_traps, array($countLocation,$i));
                                array_push($inArray_traps, $i);
	    					} else {
	    						array_push($saved_traps, array($countLocation,$this->getTCVnumber($i)));
                                array_push($inArray_traps, $i);
	    					}
	    				}
    				}   				
    			} 
    			else 
    			{
    				if ($firstTrap != 0) {
	    				if (in_array($firstTrap, $inArray_traps, true) or ($firstTrap < 1 or $firstTrap > $totalTrap)) {
	    					dd("Repetido o excede el maximo");
	    				} else {    					
	    					if ($templateType == 1) {
	    						array_push($saved_traps, array($countLocation,intval($firstTrap)));
                                array_push($inArray_traps, intval($firstTrap));
	    					} else {
	    						array_push($saved_traps, array($countLocation,$this->getTCVnumber(intval($firstTrap))));
                                array_push($inArray_traps, intval($firstTrap));
	    					}
	    				} 
    				}    				   			
    			}	
    		} 
    		$countLocation++;   		    		
    	}
    	
    	if (sizeof($saved_traps) < $totalTrap) {
    		dd("Faltan trampas");
    	}      
    	
    	foreach ($saved_traps as $trap) 
    	{
    		$location = $trap[0];
    		if ($templateType == 1) {
    			$trapNumber = $trap[1];
    			$trapLetter = null;
    		}
    		else{
    			$aux = explode("-", $trap[1]);
    			$trapNumber = intval($aux[1]);
    			$trapLetter = $aux[0];	    			
    		}
    		Trap::create([
    			"id_client" => $client,
    			"id_trap_type" => $templateType,
    			"id_trap_location" => $location,
    			"trap_number" => $trapNumber,
    			"trap_letter" => $trapLetter
    		]); 
    	}

    	ControlTemplate::create([
			"id_client" => $client,
			"id_control_template_type" => $templateType,
			"total_trap" => $totalTrap
		]);

    	return redirect('/control-template');
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        $template = ControlTemplate::find($id);
        $client_id = $template->id_client;
        $type_id = $template->id_control_template_type;
        Trap::where('id_client','=',$client_id)
        	->where('id_trap_type','=',$type_id)
        	->delete();
        ControlTemplate::where('id', '=', $id)->delete();
        return redirect('/control-template');
    }

	//JSON Request
    public function get(Request $request)
    {
        $id_client = $request->id_client;         
        $template = ControlTemplate::where('id_client', '=', $id_client)->get();
        
        return \Response::json($template);
    }


    //Private functions
    private function getTCVnumber(int $num)
    {    	
    	if ($num >= 1 and $num <= 26) {
    		return chr(64 + $num)."-1";
    	} 
    	$mult = intval($num/26);
    	return chr(64 + ($num - ($mult*26)))."-".	($mult+1);
    }
}

