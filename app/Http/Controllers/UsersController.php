<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Person;
use App\User;
use App\UserRole;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$users = User::all();
    	$data = array();

    	foreach ($users as $user) 
    	{
    		$person = Person::find($user->rut_person);
    		$role = UserRole::find($user->id_user_role);

    		$object = array(
    			'id' => $user->id,
    			'name_person' => $person->name.' '.$person->lastname,
                'rut_person' => $person->rut,  
    			'role' => $role->role,
    			'username' => $user->username,
    			'email' => $user->email   
    		);
    		array_push($data, $object);
    	}

    	return view('user.index')->with(compact('data'));
    }

    public function show($rut)
    {
        $person = Person::find($rut);
        $user = User::where("rut_person", "=" , $rut)->first();

        return view('user.show')->with(compact(['user', 'person']));
    }

    public function create()
    {
    	$roles = UserRole::all();
    	return view('user.create')->with(compact('roles'));
    }

    public function store(Request $request)
    {
    	User::create([
    		"rut_person" => $request->rut_person,
    		"id_user_role" => $request->id_user_role,
    		"username" => $request->username,
    		"email" => $request->email,    		
    		"password" => Hash::make($request->password)
    	]);

    	return redirect('/user');
    }

    public function edit($id)
    {
        $user = User::find($id); 
        $roles = UserRole::all();  
        return view('user.edit')->with(compact(['user','roles']));
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->rut_person = $request->rut_person;
        $user->id_user_role = $request->id_user_role; 
        $user->email = $request->email;
        $user->username = $request->username;
        if ($request->password != "") {
            $user->password = Hash::make($request->password);
        }        
        $user->save();

        return redirect('/user');
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        User::where('id', '=', $id)->delete();
        return redirect('/user');
    } 
}
