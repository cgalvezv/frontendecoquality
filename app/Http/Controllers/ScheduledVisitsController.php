<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Client;
use App\ControlTemplate;
use App\ControlTemplateType;
use App\Person;
use App\ScheduledVisit;
use App\Trap;
use App\TrapType;
use App\StateRecord;
use App\VisitsUser;

class ScheduledVisitsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        Carbon::setLocale('es');
    	$visits = ScheduledVisit::orderBy('visit_date', 'asc')
                    ->get();
        
    	$response = array();

    	foreach ($visits as $visit) 
        {

            $controlTemplate = ControlTemplate::find($visit->id_control_template);
            $client = Client::find($controlTemplate->id_client);
            $ControlTemplateType = ControlTemplateType::find($controlTemplate->id_control_template_type);
            $users = VisitsUser::where('id_visit','=',$visit->id)->get();

            $monitors = array();

            foreach ($users as $user) {
                $person = Person::where('rut','=',$user->rut_user)->first();                
                $fullName = $person->name.' '.$person->lastname;                
                array_push($monitors, $fullName); 
            }

    		$object = array( 
    			'id' => $visit->id,
    			'client_name' => $client->name,
                'client_physical_place' => $client->physical_place,
    			'control_template_type' => $ControlTemplateType->name,
    			'visit_date' => Carbon::parse($visit->visit_date,'America/Santiago'),
                'monitors' => $monitors,
                'is_visited' => $visit->isVisited
    		);
    		array_push($response, $object);    		
    	}
       
    	return view('scheduled-visit.index')->with(compact('response'));
    }

    public function calendar()
    {
        return view('scheduled-visit.calendar');
    }

    public function show(ScheduledVisit $scheduledVisit)
    {
        $controlTemplate = ControlTemplate::find($scheduledVisit->id_control_template);
        $client = Client::find($controlTemplate->id_client);
        $controlTemplateType = ControlTemplateType::find($controlTemplate->id_control_template_type);
        $trapType = TrapType::find($controlTemplate->id_control_template_type);
        $users = VisitsUser::where('id_visit','=',$scheduledVisit->id)->get();
        $monitors = array();

        foreach ($users as $user) {
            $person = Person::where('rut','=',$user->rut_user)->first();                             
            array_push($monitors, $person); 
        }
        $data = array(
            'visitDate' => Carbon::parse($scheduledVisit->visit_date)->format('d/m/Y H:i'),
            'controlTemplate' => $controlTemplate,
            'client' => $client,
            'controlTemplateType' => $controlTemplateType->name,
            'trapType' => $trapType->name,
            'monitors' => $monitors
        );

        return view('scheduled-visit.show')->with(compact('data'));
    }

    public function create()
    {
        $controlTemplateType = ControlTemplateType::all();
        return view('scheduled-visit.create')->with(compact('controlTemplateType'));
    }

    public function store(Request $request)
    {
        $visitDate = $request->visit_date;
        $idClient = $request->client_id;
        $usersSelected = $request->user_selected;
        $checkboxCebo = $request->value_cebo;
        $checkboxTCV = $request->value_capturaviva;
        $client = Client::find($idClient);
        $templates = ControlTemplate::where('id_client', $idClient)->get();

        foreach ($templates as $template) {
            if(($checkboxCebo == "on" and $template->id_control_template_type == 1) or 
                ($checkboxTCV == "on" and $template->id_control_template_type == 2)) 
            {
                $traps = Trap::where('id_client', '=', $idClient)
                            ->where('id_trap_type', '=', $template->id_control_template_type)
                            ->get();

                foreach ($traps as $trap) {
                    $trapState = $template->id_control_template_type == 1 ? 4 : 6;

                    StateRecord::create([
                        'id_trap' => $trap->id,
                        'register_date' => Carbon::parse($visitDate),
                        'id_trap_state' => $trapState
                    ]);
                }

                $visit = ScheduledVisit::create([
                    'id_control_template' => $template->id,
                    'visit_date' => Carbon::parse($visitDate)
                ]);

                $idVisit = $visit->id;

                foreach ($usersSelected  as $userSelected ) {
                    VisitsUser::create([
                        'id_visit' => $idVisit,
                        'rut_user' => $userSelected
                    ]);
                }                
            }
        }        
        return redirect('/scheduled-visit');
    }

    public function edit($id)
    {
        $visit = ScheduledVisit::find($id);        
        $controlTemplate = ControlTemplate::find($visit->id_control_template);        
        $trapType = TrapType::find($controlTemplate->id_control_template_type);
        $users = VisitsUser::where('id_visit','=',$visit->id)->get();

        $monitors = array();

        foreach ($users as $user) {
            $person = Person::where('rut','=',$user->rut_user)->first();                
            $monitor = array(
                'rut' => $user->rut_user,
                'name' => $person->name.' '.$person->lastname                
            );
            array_push($monitors, $monitor); 
        }

        $data = array( 
            'id' => $visit->id,
            'client_id' => $controlTemplate->id_client,
            'type_trap' => $trapType->id,
            'visit_date' => Carbon::parse($visit->visit_date)->format('d/m/Y H:i'),
            'monitors' => $monitors
        );

        return view('scheduled-visit.edit')->with(compact('data'));
    }

    public function update($id, Request $request)
    {        
        $idClient = $request->client_id;
        $trapType = $request->trap_type;
        $visitDate = $request->visit_date;                        
        $controlTemplate = ControlTemplate::where('id_client', '=', $idClient)
                            ->where('id_control_template_type', '=', $trapType)
                            ->first();
        $usersSelected = $request->user_selected;
        $users = VisitsUser::where('id_visit','=',$id)->pluck('rut_user')->toArray();

        if (sizeof($usersSelected) > sizeof($users)) {
            $usersToAdd = array_diff($usersSelected,$users);
            foreach ($usersToAdd as $user) 
            {
                VisitsUser::create([
                    'id_visit' => $id,
                    'rut_user' => $user
                ]);
            }
        }
        else {
            $usersToDelete = array_diff($users, $usersSelected);
            foreach ($usersToDelete as $user) 
            {
                VisitsUser::where('rut_user','=', $user)
                            ->where('id_visit', '=', $id)
                            ->delete();
            }
        }

        $scheduledVisit = ScheduledVisit::find($id);

        $traps = Trap::where('id_client', '=', $idClient)
                    ->where('id_trap_type', '=', $trapType)
                    ->get();        

        foreach ($traps as $trap) {
            $stateRecord = StateRecord::where('id_trap', '=', $trap->id)
                            ->where('register_date', '=', $scheduledVisit->visit_date)
                            ->first();
            $stateRecord->register_date = Carbon::parse($visitDate);
            $stateRecord->save();
        }        
        
        $scheduledVisit->id_control_template = $controlTemplate->id;
        $scheduledVisit->visit_date = Carbon::parse($visitDate);
        $scheduledVisit->save();

        return redirect('/scheduled-visit');
    }

    public function destroy(Request $request)
    {
        $id = $request->id;

        $scheduledVisit = ScheduledVisit::find($id);
        $controlTemplate = ControlTemplate::find($scheduledVisit->id_control_template);
        $traps = Trap::where('id_client', '=', $controlTemplate->id_client)
                    ->where('id_trap_type', '=', $controlTemplate->id_control_template_type)
                    ->get();

        foreach ($traps as $trap) {
            $stateRecord = StateRecord::where('id_trap', '=', $trap->id)
                            ->where('register_date', '=', $scheduledVisit->visit_date)
                            ->delete();
        }  

        $scheduledVisit->delete();
        return redirect('/scheduled-visit');
    }

    //JSON requests
    public function search(Request $request)
    {
        $term = $request->term;

        $templates = \DB::table('control_template')                    
                    ->select('control_template.id, control_template.id_client')
                    ->whereRaw('(control_template.id LIKE "%'.$term.'%")')
                    ->get(); 

        return \Response::json($templates);
    } 

    public function datesCalendar()
    {
        $visits = DB::table('scheduled_visits')
                    ->orderBy('visit_date', 'asc')
                    ->get();

        $response = array();

        foreach ($visits as $visit) 
        {

            $controlTemplate = ControlTemplate::find($visit->id_control_template);
            $client = Client::find($controlTemplate->id_client);
            $controlTemplateType = ControlTemplateType::find($controlTemplate->id_control_template_type);

            $object = array( 
                'title' => $client->name.' - '.$client->physical_place.' ('.$controlTemplateType->name.')',
                'start' => $visit->visit_date,
                'url' => '/scheduled-visit/'.$visit->id
            );

            array_push($response, $object);  
        }        

        return \Response::json($response);
    } 
}
