<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Person;

class PersonsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$persons = Person::all();
    	return view('person.index')->with(compact('persons'));
    }

    public function create()
    {
    	return view('person.create');
    }

    public function store(Request $request)
    {	    	
    	Person::create([
    		"rut" => $request->rut,
    		"name" => $request->name,
    		"lastname" => $request->lastname,
    		"address" => $request->address,
    		"phone_number" => $request->phone_number,
    	]);

	    return redirect('/person');
    }

    public function edit($rut)
    {
        $person = Person::find($rut);        
        return view('person.edit')->with(compact('person')); 
    }

    public function update($rut, Request $request)
    {
        $person = Person::find($rut);
        $person->rut = $request->rut;
        $person->name = $request->name;
        $person->lastname = $request->lastname;
        $person->address = $request->address;
        $person->phone_number = $request->phone_number;
        $person->save();

        return redirect('/person');
    }

    public function destroy(Request $request)
    {
        $rut = $request->rut;
        Person::where('rut', '=', $rut)->delete();
        return redirect('/person');
    }

    //JSON REQUEST
    public function search(Request $request)
    {
        $term = $request->term;

        $persons = \DB::table('persons')                  
                    ->select('persons.rut','persons.name','persons.lastname')
                    ->whereRaw('(persons.name LIKE "%'.$term.'%" or persons.lastname LIKE "%'.$term.'%")')
                    ->get();  
        
        
        $response = array();

        foreach ($persons as $person) 
        {
            $object = array( 
                'rut' => $person->rut,
                'name' => ucfirst($person->name).' '.ucfirst($person->lastname)
            );
            array_push($response, $object);         
        }

        return \Response::json($response);
    }
}
