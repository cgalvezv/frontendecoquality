<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
	protected $table = 'persons';

    protected $fillable = ['rut', 'name', 'lastname','address', 'phone_number'];

	protected $primaryKey = 'rut';

	public $incrementing = false;
}
