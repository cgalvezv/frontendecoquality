<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitsUser extends Model
{
    protected $fillable = ['id_visit', 'rut_user'];
}
