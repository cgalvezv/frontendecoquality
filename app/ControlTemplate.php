<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlTemplate extends Model
{
	protected $fillable = ['id_client', 'id_control_template_type', 'total_trap'];
}
