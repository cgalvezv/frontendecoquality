<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trap extends Model
{
    protected $fillable = ['id_client', 'id_trap_type', 'id_trap_location','trap_number', 'trap_letter'];
}
