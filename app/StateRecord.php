<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateRecord extends Model
{
    protected $fillable = ['id_trap', 'register_date','id_trap_state'];
}
