<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $fillable = ['name', 'email', 'physical_place', 'url_plan_location'];
}
