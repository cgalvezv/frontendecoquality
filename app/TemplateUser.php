<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateUser extends Model
{
    protected $fillable = ['id_control_template', 'rut_user'];

    public $timestamps = false;
}
