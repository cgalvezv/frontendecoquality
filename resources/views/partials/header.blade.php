<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>D</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Servicio</b>Desratización</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">              
              <span class="hidden-xs">{{ \App\Person::find(auth()->user()->rut_person)->name }} {{ \App\Person::find(auth()->user()->rut_person)->lastname }}</span>
            </a>
            <ul class="dropdown-menu">              
              <li class="user-header">
                <p>
                {{ \App\Person::find(auth()->user()->rut_person)->name }} {{ \App\Person::find(auth()->user()->rut_person)->lastname }} - {{ \App\UserRole::find(auth()->user()->id_user_role)->role }}
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/user/{{ auth()->user()->rut_person }}" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="" onclick="event.preventDefault(); document.getElementById('form-logout').submit();" class="btn btn-default btn-flat">Cerrar Sesión</a>
                  <form id="form-logout" action="{{ route('logout') }}" method="post" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>