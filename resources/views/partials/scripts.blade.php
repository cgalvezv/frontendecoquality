<!-- jQuery -->
<script src="{{ asset('plugins/jQuery/jquery-3.2.1.min.js') }}"></script>
<!-- jQuery UI  -->
<script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
@yield('script')	
<!-- AdminLTE App -->
<script src="{{ asset('js/app.min.js') }}"></script>

