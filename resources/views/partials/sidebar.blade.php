<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">ADMINISTRADOR</li>
        <li><a href="\client"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
        <li><a href="\person"><i class="fa fa-vcard-o"></i> <span>Personas</span></a></li>
        <li><a href="\control-template"><i class="fa fa-clipboard"></i> <span>Plantillas De Control</span></a></li>
        <li><a href="\user"><i class="fa fa-user"></i> <span>Usuarios</span></a></li>
        <li class="header">GERENTE</li>
        <li><a href="\report-generator"><i class="fa fa-magic"></i><span>Generar Reporte</span></i></a></li>
        <li><a href="\scheduled-visit"><i class="fa fa-calendar-check-o"></i><span>Visitas Agendadas</span></i></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>