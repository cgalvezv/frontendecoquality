<!DOCTYPE html>
<html>
@include('partials.styles')
<head>
	<meta charset="UTF-8">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">
		@include('partials.header')
		@include('partials.sidebar')
		<div class="content-wrapper">
		    @yield('content')		    
		</div>		
		@include('partials.footer')
	</div>
@yield('modal')	
@include('partials.scripts')
</body>
</html>