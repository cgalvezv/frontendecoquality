@extends('layout.app')
@section('style')
@endsection
@section('content')
<section class="content-header">
	<h1>
		Editar
		<small>Persona</small>		
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(['method' => 'put', 'route' => ['person.update', $person['rut']] , 'class' => 'form']) }}			
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						{{ Form::label('rut', 'R.U.T') }}
						<div class="row">
							<div class="col-xs-9 col-md-9">
								{{ Form::number('rut',$person['rut'], ['id' => 'rut_input', 'class' => 'form-control', 'maxlength' => '9','required' => '']) }}
							</div>
							<div class="col-xs-1 col-md-1">
								<p>-</p>
							</div>
							<div class="col-xs-2 col-md-2">
								<input type="text" id="dv_input" class="form-control" disabled="">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						{{ Form::label('name', 'Nombre(s)') }}
						{{ Form::text('name',$person['name'], ['class' => 'form-control', 'required' => '']) }}

					</div>
					<div class="col-xs-12 col-md-4">
						{{ Form::label('lastname', 'Apellido(s)') }}
						{{ Form::text('lastname',$person['lastname'], ['class' => 'form-control', 'required' => '']) }}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-12 col-md-8">
						{{ Form::label('address', 'Dirección') }}
						{{ Form::text('address',$person['address'], ['class' => 'form-control', 'required' => '']) }}

					</div>
					<div class="col-xs-12 col-md-4">
						{{ Form::label('phone_number', 'Número de Teléfono') }}
						{{ Form::text('phone_number',$person['phone_number'], ['class' => 'form-control', 'required' => '']) }}
					</div>
				</div>
	   		</div>
	   		<div class="box-footer">
				<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>	
				{{ Form::button('<i class="fa fa-refresh"></i> Editar', ['type' => 'submit', 'class' => 'btn btn-warning pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>
@endsection
@section('script')
<script>
    var inputArray = [];
    $(function() {
      $("#rut_input").each(function(i) {
        inputArray[i]=this.defaultValue;
         $(this).data("idx",i); // save this field's index to access later
      });
      $("#rut_input").on("keyup", function (e) {
        var $field = $(this), val=this.value, $thisIndex=parseInt($field.data("idx"),10);
        if (this.validity && this.validity.badInput || isNaN(val) || $field.is(":invalid") ) {
            this.value = inputArray[$thisIndex];
            return;
        } 
        if (val.length > Number($field.attr("maxlength"))) {
          val=val.slice(0, 9);
          $field.val(val);
        }
        inputArray[$thisIndex]=val;
      });      
    });
</script>
<script>
	$( document ).ready(function() {
	    $("#dv_input").val(getDV($("#rut_input").val()));
	});

	$("#rut_input").blur(function(){
	    $("#dv_input").val(getDV($(this).val()));
	});

	function getDV(numero) {
        nuevo_numero = numero.toString().split("").reverse().join("");
        for(i=0,j=2,suma=0; i < nuevo_numero.length; i++, ((j==7) ? j=2 : j++)) {
            suma += (parseInt(nuevo_numero.charAt(i)) * j); 
        }
        n_dv = 11 - (suma % 11);
        return ((n_dv == 11) ? 0 : ((n_dv == 10) ? "K" : n_dv));
    }
</script>
@endsection