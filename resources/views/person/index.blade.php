@extends('layout.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Personas	
		<div class="pull-right">
			<a href="/person/create" class="btn btn-success"><i class="fa fa-vcard"></i> Crear Persona</a>
		</div>		
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">				
		<div class="box-body">
			<table id="dataTable" class="table table-bordered table-striped">
				<thead>
					<tr>						
						<th>R.U.T</th>						
						<th>Nombre(s)</th>
						<th>Apellido(s)</th>
						<th>Dirección</th>
						<th>Teléfono</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($persons as $person)
						<tr>
							<td>{{ $person['rut'] }}</td>
							<td>{{ $person['name'] }}</td>
							<td>{{ $person['lastname'] }}</td>
							<td>{{ $person['address'] }}</td>
							<td>{{ $person['phone_number'] }}</td>
							<td>
								<a href="/user/{{ $person['rut'] }}" class="btn btn-default"><i class="fa fa-eye"></i> </a>
								<a href="/person/{{ $person['rut'] }}/edit" class="btn btn-warning"><i class="fa fa-edit"></i> </a>		
								<button 
									class="btn btn-danger" 
									data-toggle="modal" 									
									data-id="{{ $person['rut'] }}" 
									data-title="¿Realmente desea eliminar a {{ $person['name'] }}  {{ $person['lastname']}}, R.U.T {{ $person['rut'] }}, del sistema?" 
									data-target="#deletePerson" 
								>
									<i class="fa fa-trash"></i>
								</button>											
							</td>	
						</tr>
					@endforeach
				</tbody>
			</table>  
   		</div>
	</div>
</section>
@include('person.delete')
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- Inicializar Datatable-->
<script>	
	$(document).ready(function(){		
	    $('#dataTable').DataTable(
	    {
	    	"language":{
	    		"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
	    	}
	    });
	});
</script>
<script>
	$(function() {
	    $('#deletePerson').on("show.bs.modal", function (e) {
	         $("#bodyDeleteModal").html($(e.relatedTarget).data('title'));
	         $("#personID").val($(e.relatedTarget).data('id'));
	    });
	});
</script>
@endsection