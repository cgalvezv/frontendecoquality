@extends('layout.app')
@section('style')
<!--Datatime Picker Styles-->
<link rel="stylesheet" href="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker-standalone.css') }}">
<!-- Selec2 Styles -->
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
<style type="text/css" media="screen">
	.col-centered{
	    float: none;
	    margin: 0 auto;
	}
</style>	
@endsection
@section('content')
<section class="content-header">
	<h1>
		Generar reporte
		<small>Paso 1</small>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(['url' => '/report-generator/two', 'method' => 'get']) }}		
			<div class="box-body">
				<p>
					En este paso es necesario elegir el cliente, el tipo de trampa y el período en el cuál usted desea generar el reporte.	
				</p>
				<hr>
				<div class="row">
					<div class="col-xs-12 col-md-6 col-centered">
						<div class="form-group">
							<label>Cliente</label>					
							<select id="select-client" class="form-control" style="width: 100%;" required></select>
							{{ Form::hidden('client_id', '',['id' => 'client_id'] ) }}								
						</div>
					</div>								
				</div>
				<div class="row">					
					<div class="col-xs-12 col-md-6 col-centered">
						<div class="form-group">
							<label>Tipo de trampa</label>					
							<select id="select-trap-type" class="form-control" style="width: 100%;" required></select>
							{{ Form::hidden('trap_type_id', '',['id' => 'trap_type_id'] ) }}								
						</div>
					</div>								
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6 col-centered">
						<div class="form-group">
							<label>Período</label>					
							<select id="select-period" name="period" class="form-control" style="width: 100%;" required>
								<option></option>							
								<option value="4">Trimestral</option>
								<option value="6">Semestral</option>
								<option value="12">Anual</option>
							</select>							
						</div>
					</div>											
				</div>					
			</div>
			<div class="box-footer">			
				{{ Form::button('<i class="fa fa-arrow-right"></i> Siguiente', ['type' => 'submit', 'class' => 'btn btn-primary pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>
@endsection
@section('script')
<!-- Moment  -->
	<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/moment/es.js') }}"></script>
	<script src="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<!-- Boostrap-Toogle-->
	<script src="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
	<!--Main Scripts-->
	<script type="text/javascript">
        $(function () {
            $('#visit-date').datetimepicker({ 
            	format: 'DD-MM-YYYY HH:mm ',
                sideBySide: true,
            	locale: 'es'        	
            });
        });
    </script>
	<!-- Select2 -->
	<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
	<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
	<!-- Selectores -->
	<script>
		$('#select-client').select2({
			language: 'es',
			placeholder: 'Seleccione cliente',
			ajax: {
			  	url: '/client/search',
			  	dataType: 'json',
			  	delay: 250,
			 	data: function (params) {
			        return {
			            term: params.term
			        };
			    },
			    processResults: function (data, params) 
			    { 
			        return {
			          	results: $.map(data, function (item) {
			              	return {
			                  	text: item.full_name,
			                  	id: item.id
			              	}
			          	})
			     	 };
			    },
			  	cache: true,
			},
		});

		$('#select-trap-type').select2({
			language: 'es',
			placeholder: 'Seleccione tipo de trampa',
			minimumResultsForSearch: Infinity,
			ajax: {
			  	url: '/trap-type/search',
			  	dataType: 'json',
			  	delay: 250,
			 	data: function (params) {
			        return {
			            term: params.term
			        };
			    },
			    processResults: function (data, params) 
			    { 
			        return {
			          	results: $.map(data, function (item) {
			              	return {
			                  	text: item.name,
			                  	id: item.id
			              	}
			          	})
			     	 };
			    },
			  	cache: true,
			},
		});

		$('#select-period').select2({
			language: 'es',
			placeholder: 'Seleccione período',
			minimumResultsForSearch: Infinity
		});
	</script>
	<!--Setear valores cliente y tipo de trampa-->
	<script>
		$("#select-client").change(function(){				
			$("#client_id").val($("#select-client").val());
		});

		$("#select-trap-type").change(function(){	
			console.log($("#select-trap-type").val());
			$("#trap_type_id").val($("#select-trap-type").val());
		});
	</script>			
@endsection