@extends('layout.app')
@section('style')
<style type="text/css" media="screen">
	.col-centered{
	    float: none;
	    margin: 0 auto;
	}
	
	.overlay {
	    z-index: 50;
	    background: rgba(255,255,255,0.7);
	    border-radius: 3px;
	}
</style>		
@endsection
@section('content')
<section class="content-header">
	<h1>
		Generar reporte
		<small>Paso 2</small>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">			
			<p>
				En este paso, podrá visualizar el gráfico, y el cuadro de intensidad que se reflejara en el reporte.
				Además, se podrá agregar las observaciones y medidas correctivas encontradas.	
			</p>
			<hr>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h4>Tabla</h4>
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<p class="text-center"><b><u>INTENSIDAD DE PRESENCIA</u></b></p>
							<p class="text-center">
								<b>
									ALTA&ensp;&ensp;&ensp;:&ensp;&ensp;&ensp;> 80 % cebos con V y P <br> 
									MEDIA&ensp;&ensp;&ensp;:&ensp;40 % - 80 % cebos con V y P <br>
									BAJA&ensp;&ensp;&ensp;:&ensp;&ensp;&ensp;< 80 % cebos con V y P
								</b>
							</p>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-centered">
							<table id="table-test" class="table table-striped" align="center">	
								<thead>
									<tr>
										<th style="border: none;" class="text-center"></th> 
										<th style="border: none;" class="text-center"></th>
										<th colspan="2" class="text-center">CONTROL ACTUAL</th>		
										<th class="text-center">ACUMULADO</th>
									</tr>
									<tr>
										<th colspan="2" class="text-center">RESULTADOS</th>	
										<th width="10%" class="text-center">CANTIDAD</th>
										<th width="10%" class="text-center">%</th>
										<th width="5%" class="text-center">%</th>				
									</tr>
								</thead>
								<tbody>
									@foreach($dataTable as $object)
										<tr>
											<td width="5%" class="text-center">{{ $object['abbr'] }}</td>
											<td width="15%" class="text-center">{{ $object['name'] }}</td>
											<td class="text-center">{{ $object['count'] }}</td>
											<td class="text-center" height="">{{ $object['percentage'] }}</td>
											<td class="text-center">{{ $object['historicPercentage'] }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-centered">
							<table class="table table-striped table-bordered" align="right">
								<tbody>
									<tr>
										<td width="20%"><b>INTENSIDAD</b></td>
										<td width="20%" class="text-center"><b>{{ $dataIntensity[0] }}</b></td>
										<td width="20%" class="text-center"><b>{{ $dataIntensity[1] }}</b></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>										
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-12 col-md-12 col-centered">					
					<h4>Gráfico</h4>
				</div>
				<div class="col-xs-12 col-md-8 col-centered">
					<div class="overlay">
		              <i class="fa fa-refresh fa-spin"></i>
		            </div>				
					<canvas id="chart_canvas"  width="540" height="220"></canvas>				
				</div>	
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h4>Observaciones</h4>
					<textarea class="ckeditor" id="editor1" rows="10" style="resize: none; width: 100%;"></textarea>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h4>Medidas correctivas</h4>
					<textarea class="ckeditor" id="editor2" rows="10" style="resize: none; width: 100%;"></textarea>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h4>Análisis</h4>
					<textarea class="ckeditor" id="editor3" rows="10" style="resize: none; width: 100%;"></textarea>
				</div>
			</div>	
		</div>
		<div class="box-footer">
			<a class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Atrás</a>
			<a id="btn_download" class="btn btn-primary pull-right disabled"><i class="fa fa-refresh fa-spin"></i> Cargando...</a>
		</div>
	</div>
</section>
@endsection
@section('script')
	<script src="{{ asset('plugins/doc-generator/sweetalert2.min.js') }}"></script>
	<script src="{{ asset('plugins/doc-generator/pdfmake.min.js') }}"></script>
	<script src="{{ asset('plugins/doc-generator/vfs_fonts.js') }}"></script>	
	<script src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
	<script src="{{ asset('plugins/chartjs/Chart.bundle.min.js') }}"></script>
	<script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
	<script>

		$.ajax({
           	type: "GET",
            dataType: "json",
            url: '/report-generator/getGraphicData',
            data: {
            	client_id : {{ $dataStepOne['client_id'] }},
            	trap_type_id: {{ $dataStepOne['trap_type_id'] }},
            	period: {{ $dataStepOne['period'] }}
            },
            success: function(data){            	
                renderGraph(data.labels, data.dataSets);
            }
        });

		function renderGraph(labels, datasets) {

			var chartImg;

			//Chart.defaults.global.responsive = true;
			//Chart.defaults.global.maintainAspectRatio = false;
			Chart.defaults.global.elements.line.tension = 0;
			Chart.defaults.global.tooltips.titleSpacing = 10;
			Chart.defaults.global.tooltips.bodySpacing = 5;
			Chart.defaults.global.tooltips.bodySpacing = 5;
			Chart.defaults.global.legend.labels.boxWidth = 15;
			Chart.defaults.global.animation.duration = 1500;
			Chart.defaults.global.elements.point.radius = 2;
			Chart.defaults.global.elements.point.borderColor = 'rgba(255,255,255,1)';
			Chart.defaults.global.elements.point.borderWidth = 1;

			var chartLabels = [];
			var chartDatasets = [];
			var chartConfig = [];
			var chartCanvas;
			var chart;
			var borderWidth = 2;

			chartLabels = labels;

			chartDatasets = datasets;

			chartCanvas = $('#chart_canvas').get(0).getContext('2d');
			chartConfig = {
			    type: 'line',
	        	data: {
	            	labels: chartLabels,
	            	datasets: chartDatasets
	        	},
	        	options: {
	            	//responsive: true,
	            	stacked: false,
	            	title: {
	                	display: true,
	                	text: 'Control de Trampas'
	            	},
	            	tooltips: {
	                	mode: 'index',
	                	intersect: false,
	                	callbacks: {
	                    	label: function(tooltipItems, data) 
	                    	{ 
	                        	if(tooltipItems.yLabel > 0)
	                        	{
	                            	return data.datasets[tooltipItems.datasetIndex].label+': '+tooltipItems.yLabel;
	                        	}
	                    	}
	                	}
	            	},
	            	hover: {
	                	mode: 'nearest',
	                	intersect: true
	            	},
	            	scales: {
	                	xAxes: [{
	                    	display: true,
	                    	stacked: false,
	                    	scaleLabel: {
	                        	display: true,
	                        	labelString: 'Fecha de control',
	                    	}
	                	}],
	                	yAxes: [{
	                    	display: true,
	                    	stacked: false,
	                    	type: 'linear',
	                    	scaleLabel: {
	                        	display: true,
	                        	labelString: '% Distribución',
	                    	},
	                    	ticks: {
	                        	beginAtZero: true,
	                        	min: 0,
	                        	max: 100
	                    	}
	                	}],
	                	gridLines: {
	                    	drawOnChartArea: false,
	                    },
	            	},
	            	animation: {
	                	onComplete: function(animation) {	                		
	                    	chartImg = document.getElementById("chart_canvas").toDataURL('image/png');
	                    	loadConfPDF();
	                    	$('#btn_download').html('<i class="fa fa-download"></i> Descargar Reporte');
	                    	$('#btn_download').removeClass('disabled');
	                    	$(".overlay").remove();
	                	}
	            	}
	        	},
	        };

	        chart = new Chart(chartCanvas, chartConfig);

	                //--------------------------------------- PDF
	        var docDefinition;

	        function loadConfPDF()
	        {

				var date = new Date();
				var y = date.getFullYear();
				var m = date.getMonth();
				var d = date.getDate();
				var h = date.getHours();
				var i = date.getMinutes();
				var s = date.getSeconds();

				var n_date = (d)+'/'+(m+1)+'/'+y;
				if(m < 10)
				{
					n_date = (d)+'/0'+(m+1)+'/'+y;
				}

				if(h < 10)
				{
					h = '0'+h;
				}
				if(i < 10)
				{
					i = '0'+i;
				}
				if(s < 10)
				{
					s = '0'+s;
				}

				var n_hour = (h)+':'+(i)+':'+(s);

				/* ---------------------------------------- */
				/* CAMBIAR TODO LO DE ACÁ */
				var report_nro = '24';

				var fillColor_tab1 = '#248cc9';
				var name = 'Viña Lapostolle';
				var place = 'Planta Apalta';
				var bait_num = '85';
				var control_date = '25/09/2017';
				var reg_num = '15854125';

				var obs =  document.getElementById("editor1").value;

				var corr_mess = document.getElementById("editor2").value;

				/* ---------------------------------------- */
				docDefinition = {
					// a string or { width: number, height: number }
					pageSize: 'A4',

					// by default we use portrait, you can change it to landscape if you wish
					pageOrientation: 'portrait',

		  			// [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
		  			pageMargins: [ 40, 60, 40, 60 ],

		  			header: { 
		  				text: 'Sistema desratización web --- '+n_date+' a las '+n_hour, 
		  				fontSize: 10,
		  				margin: [10, 10, 0, 8]
		  			},

		  			content: [
		  			{
		  				columns: [
		  				{
		  					image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABkCAYAAADDhn8LAAAgAElEQVR4Xu2de3xTVbr3vzRpbk3aNL0DbWlLuSMXRYQ6OFwEcfTMKI7jhaMOjjPjBUdmnNHz6pzjUXF0dGRGlDNHBOV91VERHMURRSjIHbnUcimF0gttSXpN0iZNsts0ff/YzW7TpE2LLUXZ38+nH5qdtZ7slPVbaz3PetbaQ9ra2tqQkZEJSUS4AjIylzKyQGRkekAWiIxMD8gCkZHpAWW4AjIyfcFrsxARUUubUAXNtR1vqBIAGKJOpk2RhkJv7N7IRcQQOYol823x2iwMaTkWKIhwqBK+E2KRBSJzXgheF5FNZ/BaD4QrGhZFVDrorrwohSJPsWT6jNlcA/bDKDxlnHOMpE1vAqC8IZKWNgMA1S4dAEk6Fz/J3BpooLkGgLrWqRyoGtZ+sRxjkoIrMqMwaC8e11geQWT6xKFjVVTUig3cLwaAkcozTEgr47WjC7EodaR4XYwwWElXVNOmcDAx4aRU1tZkIjbKyvGqK9lcPxGdwss1xpN8ZR8LwG0z40lJ0IT49AuPPILI9ArB6+Ljr6r5+uwQhsWZGK8poikS0hXV1HmUYktqOs2suDiAIEHQYodII/86dyN1LZHcHbWRYYYzJHqGk04VbQoHbic4hkTw5pY67pmtYujQxB7u6MIgjyAyYRG8Ll7f7ODwWQ/xujZuH3WEicZ9rDu3hG/K9URHKbjGmMectB092mlpVQCwp2oBZ0mmpD5Geq+xqTWgrFbRyi9nGxiVERdk50IiC0QmLCs+riS/opUodYdvMEw3hNM2sVHH68QmtCx7FYm6SgDa1H7fIpCWVgXO1nQe23u9dK2zXT9Ngo8odQRP/iRhUKdbskBkeuSDbaW8nwcGTWAjdnh8gHj9juwSfK0uJhq+kgTSHY8ceYEmwcdYUwWLUz+hashVHK5OxCbA1+Zhkk0/8bo2lt+ViFqp68HqwCH7IDLdcrq0nrX7xZ7cLwjae/fkGCWLk95ibHITACbFCQC8AijVHVOnrkxWHGIPUxnBWQCS2/bzo0Q45riGEZzloCeHUqtP+ozjllZe3+xg6Y2DIxB5BJHplgdXV5BfLqDXDJGuOT1tLEo7xa/Hr0I9pEm6LrRFdWNFRD2kiX3m6XxtnUW+MIGqBi9OTxtPXfYOc4Zvl8pZvFeyu2IS5iYjuxvG0ST4cHraWLskdlD8EVkgMiHZsNfOM/+sQd/FP5ifdIonL3+x23rdUWZP5669f8Qp+ELanJl0iFUF81k8fBtJRgdun45VBfOp8SbgFHzMG6PipSUjurU/UMgCkQlC8Lq446/V5Je5GZmiRuetocabwAuTVzPVtCVc9ZC8eOJZNpZnhysmkRlVD0BJU8eo8ca/R3PFxOQeavU/sg8iE8Sxk43sO9nCqzM3sjBzA/VOI5trH2ay9v8CfW+gr+77N/5enI1O60Wv62hyTpe3+0pRsHj4Nv6Qd7N0aUjDXqDj9YVAFohMEG/ub2FOwi5uTv4juCBWkUxcm+iE+9xV4aoHUFEXz4riOxGa3QjNYGtoQa3SAqDTdl9vf3kcMyJbeG3Ky/x87x8AWL4thbUTbBiNsd1X7GdkgcgE4HD7yC8XeGPcXymsn4CTZErsw5mfsjJo+5DS07NY1GpYc/zXWKojQd3h0ONq/90OKpUYANArOwIBIi38o+QqHpi8hPGquWytncxHx4z8vMLLjRcwp/HiyQqTuSgoNHuoK69iU+XNrDl+C8mRBZx1prHFcj1RbVUoPR0//UFzcxvNzW1YXb6gnzz7CA6cHcubsx7FpG0Gj4uvDnYS2gVAFohMAMOEr0jVVfD8yZv5zZTXGR5t5vaMNzjrTGPxrjcQmsNZ6EfaPDx6+PcMjzbzxJg1gIk2T3G4Wv2KLBCZAOJ9uayc8RxWWzR/y/slAHqllxuHb2RO/Jc0tg0NZ0JCEGCb9UrQeMIV7Za8qjHc8uVqfjt5Jdt+dC/LUp8NV6VfkQUiE8SYuOPcOSGXsYliRq5WUUOG/jhLxr5PgtocrrqEWg2LMzaD51vkUqntbKi4nFu+XM0D+5+myqak1WkPV6vfkAUiE4QgwKTocm5O344giA1drRav9wVBgN9OXsm81P0gdPKsBWPg616w4cxcan0GRmqPhivar8gCkQmJos3G6qJfA5BbPpvc8tnUCuL0Sq0OU7kTggArZzzH6LhzmHQRjNY38ZcZK7h33D/7LJInxqzBqKsJV6xfkVfSZQJoPrWcthNPUuqcwNmW6Zyo0vG7fctA48GkbSZdbealy19kTpqYP9XbUcUvLr3SKzXytSd/xr37/wvawvgogpEpyYXsmHc9kUmT0c7O67l8PyKvg8gEokpEaIYM/XHGqI+z/tTL4vU2D1YXWF3JzN36F+YlFjI/tZCHxzwdziJAgO/in7aNiOrdaDAluZBP5v4CtQp86t4HCfoDeYolE4DSNB1ACueOiykNWW5rxVX84chtlDon4G5NlPyU3tLbkQfg3Vl/YHi0GaG551T6gUAeQWQCaFOkoYhORumpQhDg7dKFYpi260RcbYc2uGPnnwHI1J9jZsJRbs3c0OtI15y07Tya+QEvFdwl2gvBvMRCxsQdlwTVZpodstxAIY8gMgEo9EYijFN7vSCYZ0smz5bMhjNz+d2+ZXxQsqhPI8nCjK96vU6iVnWMcBcKWSAyQajS7oD2aNVc09e9W8dQ2zHFNnJ96nZ6iyDA1PgTjI6qhyGhP+OIcxyVjaLf4Uu+noiYy0KWGyhkgcgE4U2+CUV0MoIAL17zFItGbutVSDZdbSZDf7xP/kVf8Av3QiILRCYItVJHZNpihGaxl39n1n1MSS4MK5I8WzKlzgk9lumMWg1rT/+MU/XDQod6BSMJEQ6ih5jxapJRDL8zlJkBRRaITEgUI54QR5FmsSF/MvcX/HnmW5h03TeZKbFVJGt7F7r1Y2nofp/5lORC/jn3ftQqiBzbu3ByfyMLRCYkCr1RapSNDnEd4/eTnubFSX8KOZKM1jfx7qw/9GmlWxB6cNIFI49f9gFj4o7ji52MKuO+UCYGHFkgMt2iyriPyDTxgDf/dGv+sF2YYhuDyqbqKgLCsb0lJ2k7U4xlIZ10Z7MbAPXUdSFqXhhkgcj0SMSV61FEd+xDT1CbSVebxQbt/xGMpBvObwOVWg1TTYWBkTLByLzU/dwy/H1aJ719wSNXnZEFItMjaqUObc4X+BSJkj8yLj5wIdAU28gdGZu6tRGOUHVvz/oYzYRHB8Ux74wsEJmwRMRcRvT8L6WRZE78l2KP3+YBj4bZcQeZk7a9z9Mr2v2QnKTtHVEywci8oZ+y+MaJqCb0/fyt/kZONZHpFRExl6GecQRf4e3MTDzR7lhrMMU28sDod89LHH78G6viIu1MNpxm2T1Xohq9MFy1C4Kc7i7TZ5pPLeeqF6eSVzWGRSO38eG1930rgfiJTLkGxr0yqD5HV+QRRCYsgtcV8Fo9+gle/00Vv/h/jTQ4ej6Ttzf4Yiejyn6UiEH2N0IhjyAyIeksCrezY3hwuVpoFpzEaJQ0eLycPn2Wkb63SGrbgtJTFZCo2NOo4m5NxKkZR+SwW2lNXoQvyoS+zQGAVh+Y7ThYjz5AFohMV0IJw+VqAaBZcOJxCTR5xDL1deJ6SF29GwU2It2lGJtzARgaVYs+0hpg29liwkkyQtRUHNpZJCSJTn+URhSARqdGpdaj00VKdfxiGSyRyAKRCcIvks4C8YsDoMnjor6uEV+riwa7uI+jvOw0ADX2CBobAsPA0TFDSTSKzxcxJk8mSqPGZBSbXWeRaHSiGDqLpPNoMhgikQUiI3Gw2IVRpyA7RR1SJHQaRWgXCu0jia9V/N0vmFDEGMUUlcQolfTo6N6MHgySOJAFIuNw+zhU0sS67VbWbbey5sF0lswRG29X57yrL0K7YDrjF09X/KODH5VaDxAgCC4SUXRGjmJdwjy4uoL3dlqxnutIFozp9HzArg1UbRRfC15Xp4asDyjTWUSd6ep4d+ViEEMoZIFcwjxyQyL3zIljS34jT75jAcFHKBxuH69srmHm6Chmjzf02Jj9Ivq+IAvkEiY7RezVp2XpePmjaqy20BvRl66pYN3GKohVcfrVcVK9SwE5F0sGh9snPdQmFN+UC6BTguBj18nQjx9wuH2szQ0M634fkAUiE5bkGAW4vODykpEU6FT7OVTSxP2ry3G4Q0/TvqvIUyyZsDxzWwpVDa3kZGuZPd4QsszeU00hnhL13UcWiExYpmXp+ObPo3osszkvMNz7feGCCUTwurB7qnC1NEjXjJokYrV9P2vV5jbT3Oo5b1uh7oU+2ghF5+mFQXvpzF63n3CwJ6+BrBHa7933HnCBCF4Xhys3YXGcor75XND7caphZMZNY8rQ8Pn/J6pzKbXmU+8qRyBwESsqIp4YTTzTUn9Mkj4zZH2b28zJ6l1YHKdwNtuCbKjREadLY1jMGMYlXtOr2Hxlg5d12+pYv7+RkxUd9uJ0Om66SssjNyRKUZ8HV1fw/OJhUiMqsgg8/o6Zx29KYlpW+M86WOzi+Y+qefXuFFISAvdwO9w+1u+z88E+G2csgvQ02cyhESycoufuufEMjzn//27/Zz9/51CyU9Q43D4KzR625Dfy9Hpxu62toYX/eNeMvtNaitMjdhpDDYoAe2ZHKw9fawr6Hn427LVzqtSJIVr0eRpb2hiTrGHRTHE1vsgiUGltRq9R9Opvd76c/1+sF1Q7S9hatIYmXx20N+LJQ+eiU8Vid1dx1LKF+uZz1FvOca6hkGuzfxWyUQpeF18W/S9mV6F0baRxOhmmKQCUWvM4Yz9Ak6uOj08VkjP8Z4xPmhNg40R1Lnsq35deR0XEc1nSfIza5AAbZlchZlchBdW7mZd9b7diA1i5qZpH37XQXC2QkxPHf/40mRSjCoDDxU28t9PKqo9reHbJMABWfV7HnPEGahxeth5z8OnhBpqrBR6/KanbzyiyCHz6dS1fHBf44kgjCD5evTsloMzaXCtPvlOHpdTGpCti+elMI0MNCsyOVjZ/4+TJV8/y9Poq3l06QmpgvWHDXjuHylzsOuliT4EDbM3SvRaaPdy+ogxbQwt65RCsalEUFmtLkJ2qBi/P7+tIQUlJMjBjbCROb/c+S4PHx2eFHvbkVYPLS9Y4A7+7MRHB62LtNheJBiWmGAVOTysrN1Vz3RXGAQk/D1iqSbWzhI9PvSC9joqI55aJTwQIoHOZJlcD2fHTuWHssgA7gtfFh8eWU+MsJkoXQ5OrgR9m3R004nx6cgVFdQeI0omnf/949GNS484zb2ZH8TrpvTjVMG4Y+2iQGEtteXxZ8veAa53tdGb5xiqefPUsxKr48HcZIRuew+3j8bfPseofZtApURkU3HB5DE2Cj+3HHTQ7WgH4+m9ju+0FN+y1s2JzndhA26lYO1EaDVZuqubhFWUArHosi/sXxAfZ+J8v6njgheJu1zEcbh+jHzqDpdTGh8+OZtFMIw63j6VrKjhT3cLBM86Q9+qfUr6908oDLxSTNc7AmVfHBX0+/ntYeRaA3BdGdevsd2XovadJjEPygdbmWslIiqS0uoWEhFb0ERrGJEby4f4GbpoV961GyVAMyIRR8Lr4/NRKmlwN0s/MEbcENcgkfSYT4ubQ5BJ9gaK6A+SZNweU+bLofwPEkR0/PeR0bFrqj6FdaE2uBk5U74B2Ee4oXhfw3tUZi0OOVBmxUxhpnC7dT5Orga1Fa4LKbT/h4Mm150Cn7FYctPshr92XyuO/SAWXF71yCBsezeDzJ7L4/MmskHW6smimkd3PjGTNg+lga5aeK0776PLwW2Lm7O8WJ4UUB8D9C+LJyYkDWzN//bR351YZtBG89VA6u58ZybtLR3RbxqCNQB3Z0Yy6C/PevyCerBFacHn5cG/vnjG4/YQDS6mNh68Tv5fZXIPQ4mP2eANVFhc/HBlHbVUde4s8LJig5JXdwccRfVsGRCAFNV9R2dAxHUrUZ5EROyVk2az4aQGvj1q2SElypbY88i1boL2x0kkIXUnSZ5IdPx1XSwOulgZiteI05GDFxwHOeJpxXI/TpilDr4NOn1dmO8KJ6tyAMsvWWcDl5eYfmno1ZZk/qaO39Deg4SYVqCPEn14wMV0jLtZ1wu5qBcGHaZiGRxYN77YuwA/Gih1Cfnnf98amJanCFekVv1kgnqL43k4rlQ3ecMV5dXMdqiQ1P50h/o3N9T7GDO8Y/dbvs1Pt0nFZhpasBAVN5v5/hnrv/nf6SEH1bul3V0sDKdHd95ZGTTJRuhipEVc2FGJ2nALgSGXHcTCulgZMupQeG/e12b9iRvpPmZH+U8YlXoPgdVFUdwBdZIxkI8Uwutv6ALHaoZh0KQGiKqzp+D4Hi13kFzhAp+ShhaF77K5YG1rDFQmL0xNsY1qWjtwXRrH/z2PCTi38jnNVN+kkPRHqs8+He+YlYBqmwXrOw0c763ss63D72LjPzu05sQGRMf/fUtueI2l2tJKdoqbG3YsT6M+DfhdIqS2PGqf4sHdXSwMer1PqzUOhVupQowsob2kQN9+U2wsCrsfp0rq147c1J2sJc7KWoFbqOFO/H6v7nDSqAKTE9BzPB9CrYvF4nVKdcnuBNKptyW8ElxdTrJIrMr/9fuxvy+zxhl45p9GRg7+IZ9BGcNssMZX+b1/0LJC3d1rB1syDCxOkaxPHRlNe66SywYvbCT8YG8UVI3Rs2Gtnb5GHOb30a/pCvwvE0nA6aH1BpRA34HT3A+Dxdiw0Wd1mqp0lWN0dYWF3iwNTH9coqp1lQdf8o0lPxOoCpyuulgbsHjGUWWQWpyhjR0Rd9DF/h9vH9hMOlm+sEhukrn8d2PPhkRsSIVZFcZmbTcdquy33l001ZI0zBAQv1Eodt82MZ922OpJTdNhdraQlqThV6kRlaOnVdLev9PtfzOI4FdDYAd7L/69uy9Pe+Dv/W+8qp66pTHrtR6uKDlm/O2yuygAbGqUelSL8UKxRRAV9dl1TGUn6TKra5856zeD3yKEosgh8sM/G+v2N5Bc3geAjJcmArvtcxAtKdoqaBVOj+WJbHa990siNExPoysFiF8UFDl5ZFhwcSEnQ8PDCRA6VNFFeLU4X77ohOewU83zpd6seb7ATmBo3jqiI3s3XAVKis2hu7bsz2RPuFgcapR5dZO97Gb9InEJ90P1E9dK5vlBsP+HghX/W8MU2cc0pa5yBxxclcfN0I9OydAHh4MHmsZ8k8sU+O18caaTIIgRNEd/KFUe7m2aFfjSCQRvR6zDxt6XfBaJRqgN6X6dQz5Jpr3QbxeoOf7i3a0/eFzxeAadQ3+m1E1eLPewKuae1iQr7Cem1UwiOuDR1s7noQuNw+7jntbNs/EwM3y6YG89jP0kMakCNLQOy3HVezB5vYNI4A/mHbKzdXs+f7uiYOjvcPt7IrefmH5oGbFToC/1+B13n77T7JX0ViEqhDmjcNncVNrelxzqhEOt1nDze3Br+gZE2VyV1Li/xnebsKoXYy/lHDqdn8Bucw+1j4XMl7NlTj2mYhtfvTx+QefhA8Ku5Jh44ZGPdNif/5yaf5M+t32enuVrodYRwoOn3eUKSXpw3OoV6nEI9NncVVnfvHgvcmfioEVLjtrmrqHN5qbAfC1ctgBhNPHWuwN6/awAhFM5mG5pOqUN6tZL4KPF7Jbf3aucTLu1vnvuoij176lElqfn8qezvjDgAFs8yYRqmwVJqY32nNJRXPq8jJSP2gk2hwtHvAhkZdxXaSIPUsJ2CV1rs6wtJ+kxitcnS9EajgDP1h8JVCyDDNAmNomOK5BS8Ugi5JzovcjoFL7HaZIwaMWfr8iwxtFtsESiy9IOfJPjOe51hffuK9O05sQOasDcQGLQR/Hy2GFFcu13ciVhkEcg/ZOPBH/UtGDOQ9LtA1EodqXHjpJ5br1Zy1LI16AiZ7sgzb8bWPuLMSL8Ff9vRq5Wcrt0vvdcT/s8aGXcVqcbAKV9R/f5uaokIXhcV9uMB10Yn5Eh+y8KJOlRJarA18+nX3YcpO6MyBCfwaXUK8Xl/Li+l1cHvd0WvCcyGdbh92BrEetOzB3895nz41cIUiFWxJ69Bir4B3DojNlzVC0a/CwTg2sxfB/Xcn558KVw19pa9zyOfXM8nBeJzIWZl3hVk57PC13q0IXhd/OaT0azYdRtqpY4fZNxF5w5639kPe6pOQc1XVNgr0as7/I/5o34p/Z6SoOGGy8We77UvG3u1xfRMSfBIMzxGSWyMmMp9uDh8isQf3+ve/yqvH9zpntByfvtgslPU3DzDCC4vj79j5s1cKzk5cb1a+LxQ9P7b9IGM2Cn8eNIvA3r/D44+E5TT1Jk882aW597G2MThXDd6qWTn1kmPBNj5pODPQQmNfgSvi2dzF3CyppI0o5hVesvEJ8iOG45T8KJXK6mwV/LpyRUh6wO8k/cf0u91Li/zsoNT55+/cyiqJDXFBQ6WrqkIYaWDtblWfru6OuQi3YL2HK33dlp7FNqil0rF8K06gubmNtyuVgzaCKaNEfMt1odJ/tt+wsHLH4W+h640eILvo/PoZdQFjmQAiQbRbkV9c69yrDrz0MJ40CnZuMNKcZmbZReJc+5nQAQCcPekFczJuk6aajkFL09vXcCnJ1dI0yTB66LUlse6w8t49F/Xo1crWZqzLiDf6u7Lg+08v/3f2HDsaaqdJQheFza3mb1l7/ObT0azs2Q3S6Y9wqKJ/wntU77HZn+CXq2UbPzPvt+SW7w24H5tbjMrdt3Gyep8aBfHzPSreWBGYDnae753l45AlaRm3cYqrltezMHiwClkkUVg0Uul3PtyKTlTYkImJT5yQyKqJDXWcx4WPlcS5NNsOlbLyIcK2JHfyKrHssRM3k4ni/inIsUFDq5bXhxUv8gi8ODqCuY8dloUkzqCYosQdK+HSpqw2EWbOwuCw+pb8juyZLceDX5/+ng9qiQ1zdUCr/0rMFt4w1471y0v7rYD8Id8cXlJSTIwf8rF438wkPtB/Kw7vIzPCl8NiCbF65TEtm9UqrBX4myGaamTeHDmmyHDwYLXxbr8ZXyc/7p0zdPaYcfmrqLK4UWvgvtnvBy0p4T2HLG1Bx/mSOVuaUTKjhtOqnECHq+T07X7qXKI95hsUHL9mIe4bdLyHtdMDha7WPaWmT17xHC0aZhGmjYVl7nJGqFl9f2pAMxZWoBpmIay1ycGTEO2n3Bw58sWLKU20CnFlPD23XlWm5cFM4y88VA6AKl35ouni3QqNzJFzdGS1oD6ep2SmnqwVDswxSql8O+DqytY9XENplgl08bocXracAo+acXdT9YILcmxYgZvla2Z4jI3nckaoeXqMVG81X5ftI+U975cKm1uSo5VUWL2Yal2cPd18ay8N7Xb6dfaXCv3PlvE7xYn8dKS0Kn1g8WAC4T2PRm7S9/mG8uX1HbJjxqdkMP01JuYOeJn3db3U2rLY2/ZhxTW7gywo1fHMWXoj8LuAKR9KnfQ8k9OVx2QwtAAsdpkUo0TGJMwg6szFoe105kii8DWow6Ol4sNKTpKwfxJBilUuWGvnVuePBVSILQ73G/vtHKgqIkz1S3oNUOYkqGVVsFp39r7pw8sDI2LJMWoIkYTwWUZWmn76/p9dnYWOKT6WYkq5ow3BIV+/VtVu9J5GtU1qtY1QOD0tIbc6nqw2MX7X9Vw3CLWn5KhZcns8D6Ff5X/69UTL7po3AURSFf8UaZwK9o90Tkqdr52+sNGbwgnkEudkQ8VoNcpw56cMhiE99oGgP5ojBeLDZlvhz8xcdVjvdtheaGRuzKZQeWt3HqIVbG4fZ/IxYYsEJlB5Y3cem6eYbxop50X513JXBJs2GsPe+zRYCMLRGZAcbh93easLV1TQ05O3EUXueqMLJALgH912ulto6E5fGrK94mlayoYdd9xVm6qDrj+4OoKLKU23nwgtdu6FwODEsW6FHC4fby1tRazo5XXN4tJjc3VAjcsL+HWSTquvcp0Ufec/YrLK+1mvGqcgdc217JuYxVrnswOu0Yy2AzKOsilgP9kQofHh0ETIW20ahJ8ODw+7pgZ+53av3G++E+XfG+nFatNzFRISTKw8t7E78T3lwUic0HwH3ZN+3le3xVkgcjI9IDspMvI9IAsEBmZHlA89dRTT4Ur9F2j8MRJNu46RWNbLBmJYpRE8Lp4btUWskZnEt1+Tm1ZaQnP/OMIC6ZlcvjwYZqGxBFnUFJ44iTHajVkJKqxWcp5ZdMxCk6XU2xpZXxmx3bQLV9sJ7fYw+UjxcPPWp12/rZuG8fKajh8spxRGcNRd3Pk57vvfkYZRkYnRUl1f//GHsaPSkOt9LByzRZSR4r3WnjiJGu2nqLgdDk+dTzDTJF4bRY+3lfK2IykgM/132Or086zr29j6sSRWCpLKbKqGWaKlP42W78px1zT8X1C3c/Hu08zNuPiXcS7EHwvR5C/bzlDyqjRASdjlJ46y+nKKjbvL5Ku1VttfJl7kE3Haqmva5TSwI/kl/LNGfH0k9LTp6ixC9w6dzyFJwuk4zIFr4sT5S62HymSNgM1Oho56/Rx69zx6IVG3tgSev+83W7jjLWNHbuOStcaHY3ivRw346qt47ODJVjqOu4nOj2Ny0doyT1wBACHp4Wv8s8GfO71865i4cx06dqHO47xxhYz9VYbpWXiOsSY8WMZoW9p/04Z0v18Xd0adD9++5cy30uB/Hr+SLZt3c8H20qla5v3lrBo/rSABg3w0O1Xs23rfvLqI6V9D/FxWjTxYpk2vYlEoxqjMZYfzUim4Jh48nzpqbPo0zKZFq/g7SOVkj2FWofRGMvC2ROoPHeKUHx94BtSsjNpFVwBu/t+vmAijWfL2XHSzrwfdmwci4/TsmvnNxwuczNn+lTpukItRoMMmkjqqmvYvL9IihQ1eLw8dPvVOCry2XrKjTZRL9WLi48m0dix/vD1gSj8jWkAAAHPSURBVG+4ZlJ60P347V/KfC8FMixzNC/dcwV7jx4XT/+wlFPZIp7JG9FQy44zHQfSpYwazb/PGso7G3eH3G/tR/C62HmkiisnTYL2Xr3NWU9S1BDO5HeMSrSHNA8eLWbkpOyQdk6Ui40wXR8hjQgNHi/6tExMnlpOVzUyKjlw6+kPZk1m/tShUvnOODwtJKWN4LZZoxkztOPs4SH6OJYumsg7Gzse39CVVqedE+Uu6loiMUVGBNnvzaEU32e+dyvpgtfFpo928NlRC9fMnolBG0FhSRO3LZjBtCwdY5Mi2VVZB+2HJlc2urkx53KSTHvFB9K097CCStzSGqXRsXXHXvLPWpk9NZvZ4w20Ou1ok4dzx5zLAKh4dzcOtw+DJpKj3xTwq782MSndxGMLgh/XoHA0MCxzHLfOzaDVmcJLn4hHDMVolCQPNXD5xBzOufVE2E8S3UmwG9/eQ+34aKZcc6V0bUyWeGSnQRPJ2dOl3P/fZUy8bDSP3XE1MRolbU4zsSljuTJTTWVjx7bZKI2OjCQxvbzR0dhxPzkd9+P/Lvf/dxk5N17L/Tk9P3ri+4q8DiIj0wPfyymWjEx/IQtERqYHZIHIyPSALBAZmR74/zAQmpX5YbMhAAAAAElFTkSuQmCC",
		  					alignment: 'left',
		  					width: 100,
		      				height: 50
		  					},
		  					{ text: 'INFORME DE DESRATIZACIÓN', fontSize: 18, alignment: 'center', color: '#034694', margin: [0, 10, 0, 0]},
		  					{
		  					image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALYAAABVCAIAAABrQol/AAAgAElEQVR4Xux9d5hW1bX+u/bep3x1KlPoDB0VAUVAmiggoibGgkaNeqMm1yQajUaNiRqN6eamqDE3iaZa4lVjCyIiTURARRSldwaGGWaGKV875+y91++PbwYxN4o3am6e+8v7nGeeOfOd8p193r3W2qsNGWPwL/wL7wYREVHxd3G4g/+F/9/xL4r8C4fBvyjyLxwG/6LIx46iUmfm9z/gnxb/osjHBWYu0sJaW/yFuwGguEogooOf/tNCHe6Af+FDocgJId41FYlICGGMKf7dWnvoCuKfDf+SIh8XrLXW2oO/HypFirtE9N/Z80+Iv1+KEPBPLR//VyGEEEIw8weRDQdZcpBS/1Q4PEW4+xmJQdy1awgMKAb9/0oTJkAQM8MyEYGZhDiUEHv27GG29Xv2LFm8uLW1NZlMHLQ5mOE4qlAoENGECRNGjDhCSlldXa2U032ABcCWQeDiyJNga0EgIYrX+eAjz9T11rh7Vov/yen0/t5VQ9YSijJDWQgGCBEhT7BA0kL9M/L+Y4MlgEBgASuooCM21pHSkwqCADQ2NSxbttRzvYaGhhcWLjxx2rRkMhkEgRCiM9NRfPFFjRPzfSIhpPC8mJQ0b95zAwcOHjN6THt72/jx4+vqhgAMGGMNQejICKlE9ztWUlrLXZKHD331fxPEIBYwAgawAAECUAxhcagIeNc5h9hG70cRJmjqkn0EKIZkEMMSIoHi3//ZFelHCseSMAQCE6ygQhg4UrlKRUG4bfv2RYsWNLfub29v79Wr19Sp0yoqyvr2qQM4l+80WqdS5YdeKptrKwSFirKa4u7OXVva2jJvrHlt9etrXNepGzDwuHHj6wb0Ky0tM0ZrkCEhhOBuZkgiwSAGAZY4Eu9JEceQiAgCELDUJVGoWyd8WIp0gYGiILEgBmw3ZQlagcU7Iqt4sw+ye6jy+oC7H+SyH98uE4gB6norxTmjtXFcp353/e9///v6+t3HT5owbdrUmpoaKVyAVq56aU/9njPPnPP0M3+eP3/+T3/6U0GyKEiInF/96t7NWzb/4Pv/8fqaV954481LLr4UMIAE7IG2lrl/mbtk6ZIelT2+8MUv9erZy4QRrGRJbKxUimEhqKiD+KBVePA1HTqGDCIYCyIQINFFjkOFjv1bs/xQiryfLUIMQHRJsm4pC4KwIAMwlH3Xzf7PI5IcCStBklmQdFzxwO9+99Qzz5xz7rlf/srViVgcsNZqbQIl/eUvLd+ydcuZZ84Jg7Czs1NHxnFUUW1LiV69+u7du48ZD/zx4UmTJhU9JZs3r5/77NzRo0dfcMGFZ5993sJFz11zzVWzZ596yUWfBQuTzSvf02EgPMcQa4IFNOBCxMx7GsWh5JxkAbgAMWRxkh+c+e953juQt95663t9xgKWiAWsgBUoEAeCAgErQQpCfJDr/5+CFqyJwcYlCdDDv/v94kWLrrz6qhkzZjY0NzY1NVSUV4DYGCOlcF0nm8uOH3d8pIOGhoZJEycr5aJ7rdunT98dO3Z2dLTv3998wQUXSElSuo//+dE9e/YUCsFf/vL0/ub9nzrjnCFDhr311huPP/1UZc/qXr17BxSxAOQ7tiYBCqT+prbohmT2mFwLaUC6W9oIWAn7Hi/xg0Z6iUHayECrglaB8YyRgAEiwNAH5eD/HRDIGGVZWADigd/9dt2G9Vddc83Y48ZbNg899MCdd/7wyScfJzhgMPPwEcNjsRjAR488Jh6Pv/XWWqKuoRdCxGKxzZs333333cOHD0skEgBlMh3r169va2s7cdpJp59+xugxowA++uhRV115jRv3v3b7LW9sfNsTrrKkQutq8iOKR5SMyNPv962lJa/AKm8pYBiC4eKzWIFI4L2lzzt430UvkSYjiZVmEtKBZaUNCN3cMAL2UPdIt/47/O7Bb/bBdz/IZT++XQIxpLGOJVLu44/+ad3GdVdfc02PqlpAM3N7c/OUSZM3btr05z8/etrpnwCglFtfX//AA793XJeIevbqZdlScQXCBMD3vYrKspNOmmZMJKUz//lnAZxz9pwnnnz85JNnDRsyIghzzOx7ye/cfMf3v3fHHV+76fabbx0+egyiCLAQbAUMwCSIbJcV+t8GjZlMjNhaImgbSRKOpKKtoj6YkfCe5ioBmpChKAXv8M6T/5/w8H33PfrU4z/4yU/qBgyObKijMOYlFr8wv6Ozc2/jviVLFp955llnnnmWFM627Vs2bFivlJo2bZqjYn91nUy2rbGxcWDdIACW7bXXfmXkyJHnnD0nmUxlsp2+5wFQyq/fs2Pt+g3HH3/8tddeM6BP36/f9I5hwN0m7vvLAtv9swAYWAFIwGE4Fl0m5n/DB13RMLE1ZuOSZZnt+xOBdBhGwSoCoCJIA62YxSGznt5bCBw0eundU5XfvXuQ/PTu3fe67MewS0zd34vBIBATiJlB2aSzuX77vAXzLrzqimmzZoXCAuxBOMKt37b161//+ilnnH7C1KkLFy46Z845SrkEWbzu1q0bN27a6Hlud9TX6kjX9Kw9+qhjjclLqQAnm21fsGDhipXLBw4cePHF/wZYR8VeWr7s2Wef2bpv923f+W7Qnv3pN2//xOQZ1aUVqmCVgbSQDIAtDl2lvGvQZERSq4KLQOjex43sMXKgZS7aF8WV8t+k1wdd0UgmCfXqTx7c88zTfZFygRysBSmQCxTtEn3IMBftGu7+Wby0PeQvxcOKG3dvB2mOQw54r6/+cUMAGrCADwAIgO4xohB2XUVs1+CSsSdOPvm0T9x73z2jjj12wtHjCibngPL5QmdHx4zp0ysqas4//6I9e3a89dYbK1etklIq5W7ftq2hca+UJLoogiiKKirK+/evU1IWgqBP776zZs3+5CfPOPHEaVu2bDZa+37i/vt/ef/9999y6y2THPXoQw/ddNUN//bJOQ/++B5na1NNU64HRALsgSJw+A4/3gUGXIg4VCfsbuSnfec7NUcPkiFDIVKICBKQh3N+vh9FNLFQslIlEyipQILArYodgySTBuWFcK21QCgAgmuK7pmulRgBbveIF516RdeCABRAgO3emGAPupm7f/6veOQIUECnQKTQMwQB+yU0IaEZQnQIE6urGDJ91MVXXrV+w2obhn16VC15ceHkSVO1jiqqqy667LPxeOrll5e89trLq19/JYzyWhuwIJKOcisrUsz2IP+JCIz169Y6jqO12bpl48KFzx111MjZs88YPfo4AA89+MDevXsvufjiTEf74OFH/NfbGza9/saoqVOeXrUojLulq5vKMzYWaUghDSffe8AMIARVWwKMG5MgsANiMIOIheXDDvb7m6sgwOUuUWGASDBAoUZEpAWIu0yeYsgmEPAYxNASAKyFYDB1fVq0+JhhGaJ7qES3J+evVkgHhdM/EgyEAAjSogA4gARFYBYislG+KtWZlMPr+ifS8etv+sHt37zj1VWrMtlcoZBf8/qa44+fdManznnuuWfuvfenSlEyFXNdx3WcorMbTMzvOJGIYIwVJJLJpNZaCElkjbVvvPHGhvWbzz33wvETJk6cNPnMM8/w/MSPf/zd195cM2bUMT1790r0qDhu4sSXtu4tVMaDjjYPoiuG/N4PZQUMcVHqkyAALEAWePeAvw/ejyKCQdTlyY+6Z3wkkWcwIGyXNnEtIkZIsASycAE2yEuEslskFBlguwUGdSmaolGtGBLvkMa+W+/8gxGJrlBUVsC3cJlAxAQjZEfvtOpbOWXSlFw247ux++779YED7V+78aa5c59ub+sYe9xxLyx47r5f/7q0tMyYMAx1PO7bd8RGEV0ihBlKKmOsMcYYm0ymSktLc7nO5uZW1/N+c/99zS37L7nkMrbR/PnPHjjQdsNXb0ynyhAGgcmfPO2kN55Z0FLZkd6FpCViUnhv9/shd/27p9z7ChlCkSgWCAELCAswtIAA4haSkZXoVAiLgV+LrEKLg7w6RGUwJENZKO4a/aJoCRUKEqFASIiACAiAPCFLyArobu/yP3ojEMEBtEBBwFqjtA1NmEu79aU0eNKxQwYNKSutvPPO/xg0cFg+X/jhnXe+/dbbc86ds2/fnt/cf388HgtDPXDgkB49qoIgfIf5dOiYEiCMsUKIKIrKyyuuvvqab37zjhtuuKmubkChkEum4osWLpz37NMkVNO+vXPOmZNOpDgIsjZyZSyRLO1/1PBsj4SpKY2IBUDg93mid9/978HhlT4DBhxJgOAbuJoFSDESsAJWC2QlhxJxFgAKwmYdFCQcY30Lj+FadphdwCPyiBSzYkgLYSEEGVgIwSQiQqAoJznvUc6BFod7lx/PVtR6FhAMLWAEeRCW0FYd3xs3vQYPFhRn2HgsduGFl9x554/HjTtuzJgxqWT68ccfU64E2ZKSkn//9ytGjzqmrLRCR6YY/2e2zGwtg8QhaYiUzwdDhgwdOnSU58V79hxUWdnDmEg5FIS5uXOfaW9vvfCiS488aiTCiDwv4SfXbd3Y1Nxy5IQJUUn8QI9Y3iECUXfU5b22D4n3tUUYxViRBoRAzLJjjVFOhiiE9sghRyTCsCe5LCgkrQWVaRELwUIymAkRjCRRlERWwBIUSUswOvKNUELCIxsZhpXsAAisVtZlYyTUR/F0/2NIC0uICK5B3oEhMqS17zXXxmK9KwcPHgJAyTgQGVtIpxJzzjkPkGvWrFy58mXHccCcy+VSqZLZs88sL+/xwAO/T0hHSLLWEglmFkRMkpmJRD5fOProMaWlZcw6DAsPPHD/8uXLE4m4tTqZjO1r3LNgwbwzz7yACCzUS4tfXLb5zbUvLv/shZ85aeas5/o/1daaT20RJQbC2o9VN78fRbh79WlATCxAcbitxuyjfFK6pRoNJiugJYfCqjbHyYWFPHQESljXeo4NrYSKmHPIawjpuUEYisgKwHf8GIsg7KRQkHKUcgtBzlqborgIAQjzv8GPolCNACOgDJQBWEck2kpUc7l73PHjh/QcvOylxR0dHSNHHtW71wBGSIDW+RUrlwdhPhbzOjs6jj56dCEfVFXVTp9+8tKlLzY372NtLFsi4SgvigLP87U2+XzYr9+Am2++feEL82/62vXGhg0NDUpJ5UhjjLFRLOYtWrTw5JNPAYs7v/ODfGSoMnn7zd/sN6A/gKqetavWb6jpU9GxuakkovfRBR+ePId3nBZNCgZZcAFhmIzP/PKlWtsl37u7duqE8TdcGWzb+dLdv27Ysad2SN3YL/5bvCS5/Mf3Bm/uKHPdfFDIuFw+alQm17Fn25aS2orSHtW2PZtpbtmXyfYcMcwQNe/Zk2lrrenVNxZa3drhW3aAArP9kCr070LRgtZARHAtPFLNFLXWxPIl7hmzT3vz9ZVz584bPHjQLbfcWlPTU0mce96c6uoeK1auiCdi1mpmjB07Nl1SvmvX1h3bd8RiiaAQub50XUdrGwSB6zr5fMF1XcC2t7dv2LD+qJFjHn/88YZ9eysqSiMdGKOZrZSCmTs6DjQ27qup7jNj9qwhw4/sUVkOQJsIwOzTT1+8dElrhetstikrxfuL3A833Q5vixTXI0awJQ5gwqrUsCsuqPjk1PisKaf+8aftubbkMUOnff8GS3zaAz/yB9ZyaXL6Pd894Nj9Ot+ZUlO/c0uvy86b/L2bRl510cSvXzn9Z9+a8Ys7B5x7er8vXTj4h9dPf+I/43Omlp970unzHppw4xc7EtQmI0OS6IMnzn2UsIACHIu8AwdIsC04oq0mEZBFyK+/+voRw0dceMFnBg0a1KdPr959e/fu3W/p0qWFoFB8qVEUlZWVO45XVVW7Z0/Dls1bE8lEl5uSbTKZEEIZY9vbO447bvx5550/9y9PV1ZWTBg/IRaLFQoF13WstYANgrzrqkKh8NRTTyWSqYmTp5RVlkf5yFirg2j5ksVuzB8yfGhzQqAspfjjHav3zRcBAGgBAnwNBhkIbQ1MkOdC7YhhXm3F+nkL927ZOev88+t69i0dXPvCt39MHebMp3415uSZbzz2mFc5oO7cTz543qf6jBt98Y++He5puOfz15xy0eVi5JCxJ4xZ8vuH9731yowvXbh1wSuJwZVNr6oMm3LhZ62VfDAq9Q8FFR19BMdCAgHQVuruj2H82OMrynu2tLTub9/uPfVEMpG44t+vLJ5SX7+LjRbkGbbxeKy+fvfYsXAc1dbWYmxgDJQSxkTG8OWX/3uPHr2MKTzyyH+VlJSuXfvmylWr1q9fO/vUUxYseo5goyhSSkVRmEikMpmsH4817K1vbW1Il1dbCzfmAHj9rbU/+PEP77z3rgnjxj/x2vp8XGXIxpmKLol3PJB/9VQfAoc3V/MKEijRVBA2ENbXBE0pP7nlV39qmj3zzLu+/9L3f/70dbeNHD8WKi7hyQoFh1KDBilENggKHW11x44ZOe4YgBFXLfsbYyTyhVyH6ezTZvO7tpddet7qW+8ePm2Ck3DjAUMHYAUS+N8oQBIQOUmh4OrIWvA+TzT18LJK9OrRWzrulOkzNm1YP3/ePK2jW2+98eyzz6mtrd26ZYvnuUIAgqyUCxcuKC8vzWQ6X165OJlyAaO1CYLC5MkzjjpqbDGB+eJLLv3yl68yRpeVl9X0rGZm11VRVABgjCYSYag9z7fWbtm2afPm9ePG1Ta27pv/zNz21gObdm3jEj8UNpvLUb6QGNSztbEzlrFMVHRzSwunW0lbwNIH0BTvi8NLkaLXvCtTRHTtaxtls50/PPOCSdd+7pO3XF175NCNv3gYFuUZksIAyCQpgvIbWx6/6rqeZ8xM9+0DtjCUZW1iiqVgbaGkNlZCkhA6H7iQUlvFQoIFf0gF+neDupMQASCnrF9Z1qu21/jx4w3bY48de+yxx552+qkA/vTIg1oXmprq6+vrS8tKgkLB9+IdHdlLL71s5MjxUsrmlqZHH32kqqqHlNIYk0gkpVSFQs7340QUi8Xy+VwQFJRUuVyOiIzRjuNGkfZ9v1iFxcy+54dBANj7//AbmddVFZV33P7dJxc9s+zFpZ84Yca6Zxc1b29K+xIZQ0wHw3LdWUMfzRge3lx1DIqxYCJikLUWhnUYHXvFZb0njnnoG9/oP6xu6JjRi9/ebPJ5LuTIi3E+X7/q1TS8hJeSefvE1dfP+NqX+44eY7OFQX0GRLlCPJaIu4kd0NVDBzet3RyRhfBUAMHskPLYGvDhUmo/FhQjFtIiACQ47wjtypTj1fSsBRHD6EinS0oBXH75lwBauXKhcpQxJhaLG8OJRGLlyhUnnHBye/v+tW+uTSbTzMTgeDy54PkFY8dOGj7sCADz5j3b2Livtrb29NNOK0lXMTcaY5V0olAXjRJjjOu5RKQcNX/Bc8eMP/7sc84d3LsOAGCnHnf8vpb9lZWV2pcZZVxfWJBEVzXCwSjpP44iomtFA8kQzILJRgoZm9mzd+jMaz6D79b0q9v8yAI0NO168dXa8053jWhdvXX/gpU9hZ8mjDx9zoBPn9VzaJ+5V99cPWz4nBtuilo71j30RMeunSOmTq8ZPPSpH367o6NVFITTESVj8SCfT7JrYG133sA/EgYQFg4jIjAj61NWWjeTN9qwILJWiqLDVABhoZBfsmSJ7/lKKa01W1jLb731Zn39Rt/3Gxsb47E4g62FgDTGPv74n79+04impqb16zfMmjXrgvMvLC+v2rlzy1/mPp3N5ACbSCQ6O7Ou68TjnjEmiiJjzIEDrUKKvr37AQDY5HM1FZV9a/tFNl/Zr09Twz5Rmdb1jRKy+C6L7rKute5HMYKHpwgTDNgC0nJMOnwg95szLw4dord3/ezET5QmypKdYcfmbSl2nr7iGyVDBsp8hKbW6kC5JG0u/8rNtxfGjXgV+bZX397fo3r9gGeijk5eu33387J9xKsmCPMN+6q9kme/eL2XybPWJJUxbLmo4j6SafA/gAVLIjBrggY71WU26Q8cPJik/Nk9d61e+Wqv2pqzzj7LdZyhwwY7jtq+fYcxWrGUUoZaKyWrqmqWL1+eTpcCghlKucYEDESRrq+v3759W01Nz8997nP9+w3UOlj1yvIHHvh90/79Y8Yco6Nw/YYNkydPnTRp4j333JXPF1zPISCK9PoN6+sGH/Hq6tXVJaWDhh8pQYB1RPz4k6auWv1KmPLyZH0oySwAC4rA9l0K80PhMK4zKoYKQRZwQLAkcmG4bocgWUZ++5u7GDsKoJh0FUSvPZ35Pa9IkA9BwrXWaPYiHWVfesUS1Toe7WnW9ft8iDR5PbLc8cpaBpfAFY6bf3tbCKEch4kixiHC8h+KrqQWYgjqMDpKeRkOp0ydooR7wgknDB00JOnH337rrRdeWPCtO27v27dPOp3q6GgzxljLSqkDrW1XXnll794D7rrrP4SQjuPlcjnP8wAqLUucOG1aeXm5UrJ/v4EA1ryx+vbbb0skYn369PnqV29uaqr/5jdvveSSfwM4nw+EENZax3WbGvetfu21mtp+P/nJj2vKKiaMH9/a1jbqmKOPPXYSSVUQHHhCx50wzzGGAmkQg2130pC04A9nr77f2V36jLqir8XInA+qhd+TvYRFT3i18NPkeUye5VLInvBr4JXAkeBAgYk9yJiKpRy/MqRqdsoQS5FXwqgAVcJPUSwOGY+4Al6SusoVTfc3+8dvBDJsCFAQWY/aEFlXRmwBHHPUmNNOOe2EaSd+9tLPz5kzh8BLly7etWuX7xfTj0AEJgRBUFXV+4YbvjFw4KDm5lbfj+eyhZif+OEPfjRnznnpdIkQFIQ5Y4Inn3yypCTtOE4YhcxhFFm2lMsWiGRFRYXWWkmHLStHOURVpZU/v/ue0oqKYUcckc12fv2mmxYvmR+LxfJh6JWlZVkqYyMGgchAc3d6xkciRQ5PMGJYoKtqQ8AADuACIbiYOhRjpC08IOjeQsC38A0KEnmJuEFZBAMUABfwGVkgAKgYUJVwAB9dSXzS/i8JEKC4ngEJwcKaEBVJr0dpZyHnlqRWzF9wxRc/f9V1V99+2y1/eebPr7/+ejyR2r273hgD6moGEeqwtLTkz39+/Lbbbmxq2nfddTcOHDhoT/1ez4udd+75paWVAGsdAXCUU3xEa63WOpVMA8Iao5RbKIS+XzJu3IRsNsfMxmgASkgpZEV5RS4o/O7BPwrP/dpNN00YPynf2e5JqQV3ChN5whBp1gBZ0e0j+SiskcMveovrTyOgDQIJwXAYAaFTwTeIGAmGB7QRAgFLsARpkTJwLHIKYCQNXKBNIiIkDAhocxAzEIyMB0sozb4TtZMW8hCD6+Bz/uNIw0WJYG3CZVcW8lFbpr33gAETJk6EozzDnZ2Z004/vbq6j1IKxeJsQAihpNLGbNy4cebMmQsWPDdx4pSvfe3mp5984siRI8eMOW7DhnW9evVKpUqMiYhgLGutwzDq06fXNVd/hUhZg2w2K6UAkEwmjDGWLQkSggQTgJdeekl67sTjp06dPKkiVcrWDh50xNijR+9f8nqMtZRkJbFmAclg250l/PFSpAhRTPlhMCESkBKBRkgIFOIGRAgBYmgCCLKYmEwICMTk6650IQaIWaFLIIluNaZsl0iMAGIkNRyGBkwx3MpIMrKEgkDMQjEKBMFw0XVKUaQdasYfpFcxfUlZEBAJWOpamgkL6k77drjruxW/VVdKsy3WH6mCNYUwHNC/f7q0tHfdwIsGD/yrkSGiSIeWfSFgTESkJAnXdePx+O7de37xi3t+8pO7L7rkMgBr1qy6++6fn3POuccff3wikSCSL720eOfOnZ7nz5p1cmVlLbMurygfP36clBKAEFIIIUhAkFLOtm1bwlymJF3y+cs/N7DfoIhDrY2JgmSyrKy0fC9ZcoQXWmGtBlH3bJJMArAfenK9r6IpumIYTndbgCIlFRBjJCP4Fp5FBGQIxWIvzyCp4Zliwge5ViorIqIAJC0cA2IQsx+xa9mxnAq4NOCA+IBkEKeZHXBBckQIFCCQBkDIOIBADLB4pzooEgjVO8lHtvvFG2JLbImZWIFdMBFHgiPBoewy9UMJS/CBosTXhEhCE4xgSVBAQVgm5PP5IXWDBg0YwlaHkQ6jLmeNtVGxo4zrukQspGUYIUgIxcy+78di3o4d2//wh/uZw/XrV9911390dLbde++9b619SwjFbJVyjDHjxo0dd9yklta9gE0kYp/7/Bd6VFXv3r1t9erXfN8v9qtxXHfthrc3b9848ugxA/sNAuCQq5TvuDGANSNSJFxZbkA2DGA1WDJEsUj9o7BGDq9oivZj8U6OQdwgDhBDaejuyjwWgOlSB7Z4PIGBnIQmSAYIMY1IciC6JrpmAJAMMA640AJOiMigU6Ag4VlICwNkujVOMe3NZxxw0UldabOC0ebD1XAtBMOxCAUI8GxXqCJWtEMNHAMiRATBCBQ0IWnhAZ1AqwcrEA8RN4hkl9+fCcyspHQcR5Ji0mE+TCbi27Zu3b1759SpUwAqlupLqYzWUrrWgIh8P75795729o5EIvXCC4sOHGjfuXNHEITpdLq9Ldvc3AzAmGjs2LH9+vVbu3btdV+9prq66lu3f1tKz5pASO/RRx958803k8kUEUc6UkpGUc4yA3jxxcU1Nb3T6dSKFStOPnm6L10wjBJUkghlu6vBXZnTXbmK7zhIPgTejyKGIIFQwRTjn4C20ECTRKcDS5QKWDAVE5iNhK9BxSIDAQa0hLQoBXxGO0EAeYJglBhIIALlBCTDsQgYSiNmIItCy0BaOAwCOoCIoLiLeRIIJQAkQ6QNHKCJIC1cAwfwALYgUBwwQLtEgSEZkYBjUcKIGHmAGYrhAgbISXR6EIx0ATGAGYd6dQ92r3t+wYKNG7Y6StTv3rFu3bqmpn3nzDk/l8sJobRm1/GCMOjs6JTSiaJo8eKljqMcx9HaLlq0WEpZUVGRzeR69e513LhxWkdSOpattbalpTUM8+3tbddedw1JTJIAACAASURBVK01UFIweP/+xnw+b0yUTCZc1zVGCyF839+5a9tdd9196aWXvfDCws2bN7e0NF1yyWeZbSDYlsVzDgmGLVpy3OVTMh93jKaY3ozizbrlOTGyJp81Og4RwbiIxazXYTIhdAVSBLQh45hEBJNF6EHkYRlxkNPOGamNDzaQCkkfdMDkmhGWQpWECQJ1ICvBCU4FJm8QVSBJoDbYdtMZM6IUKQndjoyT9zQ4QhiCGFQapdqRa0MkQA7IR7ITpgO5JPyMKXRZu4ZjUAUkW9EpYJLG8xAz4F3IRyZKdSAGUY7kX2dWE8Dseq6FXrly5cUXX7bi5RfHjx/T1nbixo0biwQiIrYoFMIePao+fd7J1oIZWkdSSCGFMdZRqqWlZemLLxKFM2fOrK6uKRRyxUZX48aNq6qqGjlyVBBkTcTWsrXGsvVcx4/FDhzYP3/+s1prZus4TiwWe2HBCxdddNGJJ560bNnye+655+f33tWwb7cUMmvCQjyVV+xbgciSRTGSxgxL71nY/cFxeEVDDICLmt4QjELFsWOqB/Uti9wIdvvKVfvq9/SfPNmprux4ZgkINSdM3/bKqnhZee2oES6kdcQbixZ6DW29TzmxpKQkLtxMprNx7lI/DBJjR/mj+sW2NIlFr1toZ/I45Ti5hS+5w4ame9UeWLrSi3SmNN7z1One/mz7/GVUVV42a7a3sT7r2pLB/TySIhs0P/ZCYuy49NDegm3Q0dn89FIMqu0xfETnui29JxyFyHIukOXpsLFlx9wl1bOn++Up+cYOWrOOodTEo2qGDC4NqLBrX8OyFb5wrJTEXRYegYy1jnKM1e3t7a+88kpDw950yisUgpKSNMDt7e1a63Q63dHRkU6XzJr1qb85jLls26JFi6dOmTr7lNOMiZRyLGtXxYw148ePnzL5pL95Vmtr/bx5cwUREYQQUsoBdQM3b976wAMPDBo0cN++falkKpFIMwAlA8shmEWxlKuYo90Vkfx4bZHiFDQCAAnAMluBdseM/uKcARd+qmPTHqekrPa11396+RWn3nZV78lH//LIUyz4jD/86OVzzhs2YdxJt10T7W2H71UunfTkRV8574dfKRkwoLC39cDePRsXv2hNbtrl5w25/Ixw3fb5x346l28ef8Nnk6Vljyx86uhPTh9+0Zn/edyMao1CpX/uf97Wsb35rqOm1E0Y94nffvueKeeeeuvV/U+clG3YF7Z2PDx3wSduuKTnqScE+1r8Xj0WXHn7vkzT7B/84Nnv/OjoKz+dTKScdHm20NG8/PXXli//zM++6favXvOrP2763IoSp+K0u77ZmZQNuxonnjT1+U9euf0vz5ZyInGIYBZSFlVNz549n3n66VTSf/utNZlM5txzz2PmeDxWdF0QEbPdv383kWJrrGUSwhojBJFw6ut3lZeXnTJ7tjFRsX+m6/i5fMeihYumTZvW3Nykdd5agIVSAmBmK6RbX79TiK5sMsO2s7NzyuQpTz35dC4XXHvttc8///xZZ51VWlKey2YFSFgWDFgWQrAFFyMX/z1x5O/C4VrQEIQBgAisJVhY18CDbFyz9jvHzBp/4dmf/vYdnusHHZ3IWz+ykQB3ZoW2CDWA31/6Ra+84sIf3f5sIh46tONnv/vLDXccqEim8rrfMcf1GTVi1ZLnhw0YmnNAeaF1ITI5QIQ64KDgGa7l+Natu5/9+X2TTzwFnjtkyiTbeCBYvT7p+Ou+98vnb7qVUxWqM6NqE8///J61v3362tfmV0wdvenpZyDs7jfefGXoz4+6+MJTvvHV/zzxE7ap5cgLz2zZv18i5wjVgSjmu4l+fX538031y97MXPgZ0V7wlB/xOyHmohWSyWYc4X3hC18QwmUTgbWQkq1lNvF4QkqhdeS6aufOHTfeeL1SrjGmaMEYY1zXBSibzZxwwgl9+9ZZqwFSys3nMz+766fNzc3z58+fN2+uECKfC1LJkigKrDWWDRF83xVCCEnWanT14ZR33PFtR7lSqaqqqkTCB+A4rsfCy+u4FcqgaId09R4r9lTiD8uSw1TjAQgdGMBhUpasjbTnWBdUEes7e9qJ55y9eeUrQa4gmQAoTVYSIMDFzjWonnRM7aABG19/FQc63Qi5Yf0HXHx2j0w2eGxBLuYGieRbTy4/5huTqyaN3T33Sa9AHAkBISyxtgICCCohnMYsUr4c2nfYySeueXwuFzIRQvfYwbVzzhaeyjw4D+1BZb9+vSccGzXsa39iYW2GEFFMi56aOBcZbWJtkc529Dx6dOP2PbktB4457eT1R41pfWtD/bLVF91422u//u3SH93dvzGIeTEQHVJW2eU2JSLXdQUUS0lERoeQVgjHdV1mFlJEUeD7XqYzUyiEqVQ6isJkMllVVb1ly9Z0OiUEpkyZDHAUhWEYbdy44bnn5q14ecVNX7upurrX2rdW/+pXv+zTu19JunTEiOH19bt31+/yfbepqdH33Vw+57rKGKO1Bqzve1FkWJtUKpXNdSTijpAiBum05xKalLFWdJXoMgEMCRj+sJLk8DEaLbpqdCUjYSkembAzV1vb++qrv1p51JHtLU1MEIJgdbHiDgJGwLKFwOyLLxo9c/aaZ18QDA50ZXnViIEjRvcfkoJbe+wwL+UnI8Fs47OP2w8jImhGCxA4gBRGQAnXZS12NSd6VQ88b1ZLpq1x9UZt8zbk8tLKIUccMWLAkBISuiU/auass277eqG5+dVly51kHFJAcwgoociSluTK0lg8gZL4+vUbvB7J7KAqwdHDn/73fa+/PfWGL130yx/F/ERcW/fQdj9E4GIvVCaQYTDDRJYtqDvn3FgN2FjMaW9vGT1m1FVXXdenT68BA/pff/3Xb7zx6xdccF5HR7u1xtoIIMdxpFSLFy9etGhhaWlZ7z69+/XrX1XVI51OX3fddb7v19XVnXTSjIs+85nTTjstl8tprR1HEZHRhoQw1uTzeSEkEbTWUkjAOo5DkUZb1guNZEsMy7AEI1BMWZAf2hg5vLnqGijAgBWRZdJS+H6sZcO2u2Z8sv+lZ17y0x8u+dMjkQIcZfOFKE7kkVLkGgLjmRtu88rLzvj8v937yPNGoHHxskW3fCuBFCDHnTU91qv8zEuvULF4r5OPdfoMCHUg2HQgn0cEsDIw1pSiZM+fn9v/pfPHfmpWZbIqvT+XhozJ+KZnl75w69crUNYDMlFWuvK3D745d+Hnn/zdhC9/fusrq+BT5COCkcyKKC+0P7jP0FOnkRRDRg4D4YRPz3l7Y3vJhIF//MpXR5w+bc6Pv9d6yflv/OKeGJW4RGAYtk5oyJHb9uxqbtpbWdXTsFUkAEY3P6yFEIIBbWwhCEeNGj1t2ozXXluRSqYGDx4BYMiQ4Uq6QSEqFCKAhRCxeOyUU2Zt2bapcV9DFEYAgkIQjydqamqmTJn8yiurTjv9dD8W37Z1AxFZy0JQEIRDBg6tLu0hhHS9WGiMEpKtcZUDCEHsaKbAFiT5hhQTEYVgBgzxh+fH4byrAIBYBA9gImttQGh3KPKUF0tVjBx50llnWx2272loX7wKLLwRfSd+9XMcg21pUxAgoOFA2NEue5eZIPBdX9b19kce4w6q63vS8bUTxr36h0f+eNnnNtz7h16Dhid7VRQOdFTW1g4fO2XCJ2f5yvWAgDgi8nUht3df/2FH5fc3bXxhYQpx5fvxIf0rR4yvGnVMRiqTjLkZHd/VCqJkTVUiAhxlPPKhfSKOu51RdviJE6nUf+6aWx6/4Kptc5eV9BtAfco/8esfHnPO6SWxFIAg02FAERviYniDSsh14/6W+l1N2+uLAT4DkBRCyuKKQUgJUlobIsePJbRmADpCPt/VWorIDQKTSJRUVtQao9ete9vowsCBQ/r26R1FQdFmcD2/sXH/Qw8/NHnyCYVCfv78eaUl6eeem+/7nhASoDCIhg0cVlnVE1ZqJuG4VpDjuiSk0blMZ1ucJEG0+5KF4zCrbr9lJNiCP5ySweFsEaCrW1V37pkgkcyZ5udXCe1c/PkvNmzaveDZF/ru6Gj55ePLpDjlvh8E2fYnL/1q/rW380OGtz+21GvoKIB3P/JCr7xsemiBO7jnCV+5Irdzd9jYuvWBpzf9/E9YtWzTun2JVElVS7jh4Sdy6fiFC/5r52uvvvqrh2KOl9FaS6m1t/QXvy2k3P3LVutcTpC7fu5zNUeO+NRVV6KlffGmnXuefym7ebto6Vz/wNMdL79Bu5r3P/yXZGOniyRt2X3gsfk9VWJAafXmXz2y87+epdy+TU5i8OyTtq54dfEtd874/GWRCed/5Xb1x2f6wM9ZwQTNRgAUGRWxJBQ9m/9do2ezWSGE78e0jozpqs0kIZSSxkQvL1++Zs1qIcSll13at19dNtu2ZMniYcOHAcZaI4QsFk+EYUiCtmzZsnDh/OtvuPl73/vmd797R0NDw8FOzmBWngspdKBJkmRobVlASLVzx/bVa9/opxxXQxiWIAFEbLpaAHxEsc8PQBGCBQSgICzpSlbb/vj0yw/9uQxelMtkTb63KtUtB96642fb5z2f3deSrN8/WVRknnrx+ccWKs3eVrV+6VtVRqy9464g5gZsXMfL6iB33wOVAYZRn7a2/ItX3DYkdHZuXvfSmxve/sUfDjTscfc09ZWVBooj48HjlzcsOe8rlRQbJOJ55bzx/V9uSCaUha9tMh9t/f6vSetEZJZ+9oYe5BUsP3PNrW4hKkMy89raJWvXJ7V++Wf/2RwVqkWsLVazb8XrwYo3a0K54Vu/yjywwArasW1LDzdWppVPMjRWgBQod6BdH0gnamLkKADGspLvGplUKh1FkdbGGCOFZGsB5LLZmO+3tbX8/Od35bL5UaNHTZo4GeD9zfs7O9szmUw6VR5FOopMdw8jjvnehRde8Nyz87Zt237jjTffe+9d69evS6dTXbchGFgAypVF96VDklkDONDZ2dLZMUSWp1gqS2SNgCBY0d2p5SNhyeEVTTE2VuxXFREca1K5KK5lJgprjDMYpRFRpyvrkKh4dUtZfYfvJ3OwKhtGkc25pCKdNLbdQ96aeM4mc9prz8ciuI4nfa/JCZvjxKHpjLLlbmUv48ReXT94TzhQlBdQCMkysZHoGXkDsjKZD2NG6ijsqT2vM18IgqwOvbjn5/OJyKRErExLLzAlLGNtQaVRApTU0suFZexwJqgyKmY57sVEJvA78mlth4iks22X3LK9v0gYKYTrRjYEkSH4Qjma01r6RihBAJT4q1xaO3PmzN69e2sdKaUss5ACwKc+9alTZp+ayWTDMPJ8d8aMGVJ5bM1TTz3x4ksvbt68EYC1rJRT1FZSytbW5q1bt1x51Vfq63c//vifrrji6lmzTsnn8123MTawGoBhspGFtkJSV+24FPFYPMrkuD0bt1RseiZAVCysR1dvnw+Jw1OEGASWQATOsjGkAJHRhTbH7idTENZYIw3KIEvIh6c6BVqVyDtuTHkBUxNMu6saHbsNhaY49ituTkhEnM6bFEvBrrYM2IJ0slrD2iQlSiECNm2sQ0ewEBnYCIbAgUWnYBYqDkrD8VwvZM5FNgFfwjtAIZFgqQLWSeXYSAdgBhLkGWMcx42z0kGU7ChUwTOkyJKwyEsvG/MlU1WBRBR65FlwCCuM9TSlAo5HWLd+HRvDgHlXaQ8nEvFiPbe1lorLOmDU6LGDBw8TAvGEH0Vhe/sBAHPnPrVixcuJRGzZsmXGRI5ytTbGWACu66TSyccee3TpkoW3f+t7e/c2PPjgb4YPHxaGYbFOIhaL9e7XzwIgSCWJBExXM3nfcTwmtOepM+9aEhDFIIk4NIz3od1nh6eIYLhF7yqgY06DyDWmnJn3/ODShU8Nv/P65hLhkXCNzcNmiS1RIrKpCJkom4saY6WpUx9/ODO0rvKo4ecvfKzm7NNOuv+eyinjPdaVxqaCIB3p6jwqImG0toB11E7u2IvQDKs7/ZGHkxUVykRMtsAmJOQdtAnDbBzmuCY3E5TClZoVsYFlEh4La5hJaMsGIi9EAQjYGiJjOTLWkFSWNNuCgLGchdUCmikAJBAZEVrDBAihgASpYE+zWzAvvrzcRqF69z+KAGAMp9PpIAiUUr4fe/755xsadhY/OnCgpVDI9+vfd9z4iTt3bv7zE48rRSDevXtXLp859dTTkon0n/70p9tuv/mJJ55IJGIdnR0PPvjAvHnPXH75FXv27P7jH/+YSqWttdZyaVnZ5ImTi/k0xRiSsZYFRSZY9sLipBHl5CashGUDDrs9f8J25+99OH58IL9Isc6qWBoQWt2ZkNO/d1P/mRPXznvu2MsvGjr9pO167/ZSuTUhcrbgQkTWHKBs+tQpwz535X6Er69ctCtsqT15UvWRg5oadu175fVMfYNGmE3G9lT4bTAE00Y616u0HXqfydZ96TMl/37mtkLztpeX5Qr5Vs7pknhb3DmgdDapWkv8rKQDFIUwaZa+RoKEZqsFJMMt9kZnWEIoEUkUFKLuppNaIpJocdHpdHkLQoGKCLUFGEKTi1avK/vECCjAs3AzoSpEbtyXSlnzrm7plm3MT82cMdNxVBiGUorGxn333//rhx/+w6OPPvDLX/5S66impqaiomrZsqWZzk7X9YIgP336ialk2c6du7/0pavOPmtONptdv2FdEBTGjB71tZtuXvP66qVLX7jyyq/069dP66jo3fc817I9aFuAwEpEYJJi29sb0iHCfa0uRLH7V9QdiSxmgX0Eaub9zdWiFXIwf9oBqcCKun695szeu2rl0lt/UBsryWYL1VNOmvr966kzv/yKW4+46Cy3rtZxY71mTpBeahOaKwb3rhp31KizZ0nJJT0ra/r33ZVMtNf1m3H7jbFB1Uu+d8+OufPH33Fj8tjh7U+v2LF2zfQf3sCtbQ251poRfZeVUo8jx51803U2CP7rum/4VSVn3HTN1hfX1MSSi7/3k/JIBGzIIJQi43YVKoaya/Zo0dUshAGnGEMQEAzfoPjfFDRgCSF1JSRI7kptMaIrC8IB4po8KzqtbWppLu9R8+7hYQDJVCKfL6RSCWuN73sb1q978801QgjP84SgQYMGFgpty5e/5DiqUMgnk8n+/Qc2NTVs3Lj5C1+Y4XnuuHHjt23bGIv5l1zyWatp3LhxGzdt6N2n74wZM1999RUppTF2xowZCTelu+oRwIQQFkK0traUCDfojFRnQN2JUaCutlDoTuj88HhPKULdPTOLDg4JVmDBxkpAhtGBllrrPX7dFRvXvXn+g/ft2LwZNRXHfPf6lhFlAy/85OaXVr21cEnrhk07t2wdPn0GNG14aXW2oX3T7h19PnFitipZd/bpqbo+rz3y2IjxE7yevUecOH79228c++XLsuCmlevefn1Ncz7X98QTcgk15ksX7+3c19rZ9Ilrryzv1yt9VJ2tSvQ85+SwLKGjwBdKWY6EBSMRIcVIGMQtio2QPNOVLOcxfO7KOUoaJDSIoQUUI2SEQMwibpDQUN1JjQJQIOSjQkdm2/ZtK1asUOJdGVwEMLTreJWVFcYYKSUReZ7r+77v+0Q0ZMiQ6dNnZDKdQVAQUjDs1KlTB9YNf/755ydNmuh5LoC6AXXJVMpx3ebm/W1tB1Lp9AknnJDNZpqaGplZKWVMlEwmBdHBfzJkCVZIj9SihQubtu8uYzdhZTHV0ojuftrd/PhIKHIY72pRvkVAAFLMcSbOaRglYsl9Mhx3yRW7GhsKnZ1hR37TXxY27q4fcNakbU88u+hn355V/eNWp3TnW+uUtfn9+/du2l5TN2TXxk2QHHCYMQVj7At33dfL+hV1/Z0elUf0HU5BFDU2R+v27g4bmjdtA5CzUZgNOm1++/JlF3/+unUb1mZymab2VqstEWm2bhg6wnEM226y+9zVpV4US5EtDmpMecj0UhaaoBgHhTF159h6FmTQLuBb9LZu84FCsi7dnmkFoLRlKcAEEswEtkcdeezw4Ue++OLikpJ0EAS+FxNWRJHp17f/zbfc4vvp3/z2nnwh4yglBLnK/dYdNx439vhjjjm6OMJvvvlaprPDWv9nP/3xEUeMTKdTUonW1pYtWzan08koikpLK8vLawBIa40QDJIM10BJKJJtFKqGzsqOqOjZK6bw/VUq4odnyXtShAFZ3AwMUCAkmGNKZesb9v5laXrc0aWzJ538y+8u/c7dMjJ7Nm3sXVLTP1GdihQVCml4SRVP9autHdTPOqR9IZOOdCmhHEHKMTYZj9mYHHPxOcMr6tY8+phTnqh/Y+2gkcd0IpKpeM9037Kaao47IKTY44QzZsrkbW+/2diwx3V95Xku+Q6EdR3XysBYJgZRVgER4gyNrgjWQd4UnZ3FVP7o4JMf4g872GifAI8Bgw6HvAgl1lQ05dI5vXTF4mkTJ/XuO6Rr/Lk4d1gIAZau40dR5LluFIVKeWFgHMfz/fT27RuXL18mJRwP0qh5c+eFUThk8OBMpj0Iwm3bNq1atTzu+4qUMebVV1dYNoAVElI6nhvLZHLDhh45fPgoaEPERrIByQguUAjzm7ZtdkvisdbW2ogO/L/2rjxIruK8/77ufm/O3Z3dlXZhdaADS0gIIQkhoQMKZCRDQFEqsS0wqaTs2CEubFccV5ABE4IhNgSMy0ccV2K7IDYxkABCJgIsQAjdICEQErp20bm39przHd395Y83syyHpDUKNnb46pVqevTmzdue73V/5+8HFiAxpCBqUO9Pv3j11B5NVOJmwUzCguGbNd/5frjv4Je/+6+v3f/I/ieeWnvrt5b++V+fc96FR9c8heYOc6izBtUHHn0yXTQXzb20f8uORJ8X6+g3ew/Xh1Lv2DNC0/7Vq+WhY8u+/OX+jvbunq7DO3ZWp5KHn33urKbRmx566JyzJ00/62O59S/X+Djy0MpzahpHT5j29Hd+qI716F0H3SPHzev7VKClBtgKkCJJlfs8zQmJsIS1QMwgZPiQiRI3FcgvlMiliN1s8MFkBrP9xCeuUMqpODusdQDitvbWna9tW7v2+XyumEyms9nCQH9OG66tre/q6r355hW33XbL7bf/o+cFQiohHSKZTKbS6XQ6nU4mUjE3HgRhMplcuHChtSYKqEfrolWAREdnx/adr5zRY8SAl4f9QLFYTspqJVhAPnv13/b8z2olUilrIZF36aguxuszNem6vs7WmBekQhuraxCFQPvZ/MhM0srGntIAgjBTZeNKlIwOQiFJkdAlvyqdLvmlkl9QtTW11fXBwRYXyeZGJ+lzLOc1VGXy/T3FUfWy3yMlfBOm8yXd2FggKzs6kumagaRM+SwSsb6+/kbE3EALUAnWVwQgphFjnA5yqwRCQigQM9AERTBAy6jYvgVnXv2Fv7h60Z8wl3kgABCxMaHnle7+5zsPHW5RkozVAsp1474fum48CPwIkbehYcSkSZODwFxwwQXz5l388MMPdnR0ah3u2r2LLSulmHWlzMOWQxssraXv3ve9msxIY7UQESyiCIkSoCeeXLny0Ydnbz8+svl4LIQ6Md+IFgAhaagHuSnfu/P8ryy3bIWNMLdZnQC9ebgY8GWqLyBKG0YIBY61DW6V11tyOo41ONKVKWGCeG9eA4ripqckSQRk0yJWyJZKhUAa1yUJtmTZJbL9eRcccxKFgWLQ41UjoQVqez1jOWWF7e+LO06xvc9hCplcwWmK9XceTwhZJZOcD0pFG7MiN5CvSSXhaWIGSBCpCCi/QrfyvteSCIs8ijuFEhacMjSi33QN6DXPPTt/3mV1iRrmcgkGM7TW6XTNkiVLvv+D79XXVReLgXJEEHhSyCDwiESxWAr8cNSoMVddtYxINjY0MJulS5flcsU3Ww6sf3FDIpksFr1k0pWqUgzLRBDZgdz8BZckUzVh6JMQhohA0lpNBMhXd+5IHy9UtQ1ILwiVK05sc/AgM8P7lVPnaCLPl4ksMYyJwQmMiVlZRyqvTWBtlUyG1jNKahKutZYwICgJaIZDCS0NA1FqSYCYWJHS2kIqKxEY8tjEoYrKwgphhK9ZQpBQECyYtBUsWQilQy1IOkJIiCSR9UNhWJAybCkqtBLlSuzT2W4MlY3ZUIABSUIxuXlvRHtxW/rQ1h0vXzl/MbMWQlnLROXG/1kz54w7a/zhIy21NVWh1lIRM7OxmUzmqquWEigM9baXXz169NCMGTMWXrzoPx/8aXV1DRGWL79GSieXG9i46UWtfcdxwjCUQlmmZKrqqquudl3FxrCQIXNggrRQLrC3/XBvd+foTj+eDRIyXtAhSH1wrPKnVpHITI68GwliY5QgCeEwxwGyZEygIKERChuLwlYkYNiFMCHLqJHvLXw7grWC4Bg4DAsWLNyQWZCyHH1FEpINKwaBrTUuCzZWsQCxa0BsXRADAhQ1ZJfbRiyi9uPTESvKmOMlAcUgbQIIybamNZcZKbsOHwkv9JRytA6YSUrJTELIVKr6k5/89H333e15oVQCgOOogYGeWbNmfepT1w1e/NfPPOn5PgDf9xYv+bPa2sbo/VJpYMtLG2zI1loiSVDHe3o/vujySZPO9f0CAcyGlUMkCGQCs2bNqr7mgxd2eiI0hkSc5Wk8FKeWU5urg1/+lp9NsFTGCogxANJABRAfsTJJSrnkSTGcKKvE5WMQ9dtUmD0IZfqcaEuNgrnKIto+ZIWMIOp/oYqDGnXWmEqfZhkqp5J0fH8Hot4ClIMoLqmA4JBTm7MTPHfr6mc7uzqIRIW/jKQU1iII/alTps+5cF42m1fS8bwgDIPqmqoDB/bt2bO9v787m+3dvPmFn/7s360N8/l8V3fnPffc3X28va+vPZs9vmrVSt8rmXLlc6y3t3/RZZdf/zc3hIHHlonIcRxYK40Njfazvc2bXmoqwunKSqEsrDrpX0Sns+8Cp6qAjwzlyqRHbYBRyyQTPANPQDBKCoFExoOvwLrcnRVWlvuyLexbyAAAC7lJREFUof92D5243JfFlS7cOMpmpgNYwJMIacg9VCzQ6FeMm7KGRU0iwpbn4jSXkMgTNgSPkDBwgZJkz5o4k/SCTGtOxM2vnlz1xeu/QkTGWK3LXIhGm6qq6i996atBoF9+eXMiGWMmIpvLZ+/77j0E6bpxz/OkpMcff2zlysfDMCiVSt+45UbHcYQQ2WxWCLjxeKkY5HMDl166+Prrb4jF4oHvSSmJWFsDC1dI6ahX97ye7i7UHctp30vIpIiMr9PUgpPKMDaaClJF9DNEKQxDCACHkeQyRUQKoBCS4QJOCDMkGPWefwQDJQUwHIYhCIOQEAgkTHlRiQFlbHEAlVbeSDOiCIetRBKjE/5PgokuoyAQKNT5EECWGErKkBkca+2vG53aufv1Q0feHDd2ArO2lpktGEqpICzFYomv/d3fb9265VdPPnb02KFCIZvJ1ASBx0z5XNZx3XjcLRbzQpAxNpmM5/NZAkU08gPZPseRdbUNV16x7NOfvlapWDFfdGPKWC0JJIhYSFe1th38j18+2NBdqO0qGlfKwDos9fAejfc9OSftxrMEoDcodiHXYKgAa2B9kB+IqOPSMBOgDcigBIRcJigyFZvgRKAPUQgr0KBK5skAmmEMwihex+V6m0EVsRVXxVbYakyl6V1XsEkYsKfXPCKBwEAbZAEAJT0IuUbCN/Zwd3883LzmuXF/NZ6JjQKRUCygjYJkAyHVxZcsmr9g3pYtG7u6Ot/Ys2vXrt2JRIKZjOFiqSCEtMYoRxV6iql0ynXdYrE4on7ENddco7WeMOHsadMuBIwxQTwZM1pLqYhYC2Yw2L701FqzvzN8s9vrzycgNUIFoWFPxGoV/YgxyALQidKEfAiGCAABIWABS3QKppJTpPEAMqb2svN8ZTMcj4FKMMJBkuFoOBpGEgu4zC4AwC3DFHP5opUh3j0kikVvVoY4wVCd/DofwDA2ZJgZsvolCWNSNlGFrc++sPjqP6pvbPI4lCQYpEiRJWtsaMLe3t5SqZSpbZg375IrrljW198Tjyebm/duWL+hUCq6jhuGARHF44lzzz13xvnnhzpMxFPJVHzHKzt+8pOfzV+w7+OLFldX1wjBUkkQWWO0sULhaPvRVatWTRt7ds2oc1J5kwoRMxGbD6uh3TJvm0N2tHSMyklb7+rqc8aC2RIgILjMAnhKOTnDJhfIxIE4XDAQMtxhXfQPWGzBu/fO2984fPDOe+4ZPWpMAEOGFSQMM4xwnfvv/9nmzZv9wLvrrm+f0dgEiPXrn924ccPli5fMvmA+AIB9v7Bt27YxY8eMHTM+ssceeeQXAwP94yeMe/DBh2644YbZF8wBWGsbRbBIONlS7uu3fj0p3RVfXdFwRjnnbCpJhrcXTL5TQpSNyJCtBYE4IpqK2IXse314WKEzBgQEGQ0SCHxAaBgLFQCIcKe4XNb6/0QIUGCKyy+uuPH2b952xx3fvPuuuzOZupC1JUuSrGW/VFi+/NqqqqoXXni+vq4O4Ecfe3jjhhcvW7T4sUcfb2/runrpH4P5xz/+t2KxeODAgc9cd93lH18CoCZTu2fvGyMbRowcObKpqYmZjTWDVhYBm9au7dx/6Ef/8qOGhgYOAxLEkrns05HBCY0NCwoBA0sQ1lpJ5FC5EZSHtyWfzOklcLWMxYUDUog7Mh6XUE7ZG5VESkG5UC4rF+XDGfraKmVl9FpZ+ft+SCuNpZL1k5nqb97xrRGZunu/fVdHa6ujFMOG0CxISiml2Lt379VLlwrhWKapU6dYtsymtrauqWkMWBw/3nPGGaNuuukbjY1N+Vwxmurx48bn8/mGhjPnzp177733el4JgBAAIIT65WMPP/DoI7f8052Z+hEwhqyAIfKFCkQ8FDEjlT3hDLtapIpcU6TqEqpDkQ6QCOBqgKAl9KmDHqdQETrU2rpm3Qsck89v2PB6S/Ovt25+asM61lqBvFxpzVNrXlz7YujrbF9u9a+e2rhuow7CIwePPPHoE282HwSzENS8r3nd8y+ShWAStnJw5Rj+0P5Oh0zCUgi2juvpIJlK33LrPwjQzTfe2Lx/r5IOSdImdN14W1trS0vLlClTpHSYdXtbe11dXRiaplGjpJQA19XVL1++fNu2bVLKhQsXRlN99OjR7u7uuroRBCilXNdRUgnhtLe333XXt3fv3v3Zv/zcBefNpGTMOKQVawWtEEoEInLxTjiHBIIiSCJBMsoGM2AhzXDbfeVtt92GE4gQorm5ZeXjT0ydNu25557TYWg9P+zPjq5vrIonD7yxvzfb29zS0t/X9/rrOz3fc+MxtvaZZ36dqkpv2rRpytQp+Vz+6aefeWPPGwsWzhdSVJqYhoTjhj2kSpHM72YYLapCMJEjJNjG3PhFc+cePnJ04+aNxupRo5tSsTSALZs3TZk6dfr5M0hYKZz/+u+Hz58+fdmyT65c+ejRo8cWLFjAzFu2bPn5z3++dOlSz/NqaqqlVGedNbZ/oLelZf+xtrZrr7m2oWFUPp/dsnnLD374AynlLStumvKxydBGRkUIgowgLSgUFAgiIRTToPM4OGnRXQeSiwpaERRIgMRbWWFRScO9W4Zli0SSUkoG4Z5XXs2o2Ag3GeQLfj6sdVMohFNmntsw7sw3jxweM+6sYx1t+UJ+4uSzG5vOHMgPzBg1c+/+vb4ODh86PHHSxKJfVI6yDK7Y0G8pwbCHbxnsv4thNJMC7HBUAEaWdaK66mtfX7F9+5Z77r1346b1y5b96bix46648ioAliOEcHPFlVesfPyxw0cOdXZ23HzT55mNEFi//oVMpmrDhnVHjh654447ksmEZfu5z34+AvcLw+KmzRtWr36yva3jM5+57uKLL3HdGHsV05UgCEJARni4BAG2lWzrO244UmxVZvcu2x9vaRPjLfC8E8spVMS34Zlnj5Xp2Kv7d02bdd7CRZewHzhxRxMC7VXVZUS1u+fIgQL7YydPeH7z+n6/YBLCxiXSzs4Duzs7O5XjtueO7znSMnHsRENlrtrh6MSHTUUAxFgoIxB1gxOFxtdaz5w164H7739x3fpfPPBALl9cuHDhxIkTL5o3V0oi2OnTLpg2dWqhWKhK11TA92jFihVB4BGRkEIKJww9x3G11tu2b2pr7Vi/YT0JsWD+wlu/sTQWS2jth1bbeNkkGNwdykV0BEPsD6kmesccupZSAaGStohQx6KT3mv5eA85hYoYHeb6e2fNmL5+3fOlYs5xXUu2xIF2nFd2vt629wByxfoxCVtTk23rSFiujSdGJlIHd+9yAn3e2ZMmjZ2489VtjhfWxRMOGzWEzvj3UUS5eRUsYQUMCxYIdKCEWLzkylkzZ/T25155ZfuaNWt27d7V3LKPGRfNmTN58jkk2ZGOiWiXmaUQltkaY4xtbWt9+umngyA4//yZ3d0dtbX1X/jC9Wee0Vhb2wCYMPQAImb19vw1DeqKhSIIOqFnKayM4uJWlMFFo8tE8UkaRlnayUqKiKi35/jBQwdnz75w587X6mtrm0aNMVaDKJQUar1n+w6H5KzZs8PAf2nrS7V1tVPPm97X3b1z52uTJk8+c/QYAH3dXa2trdOmT69AEUaXrnzH8IfDWWo+uGF5cY42GTCxJVi2BAvLAgS20olXPomBgf6NG9f7vh9PxAnU1dX10ktb2zs62JYXgeqq6jlz5oyfMN5ow2yDIJw376KmplGDMY4g8B3HMcYKQdYYJcoP8+DdDbUi3hZ8ePukMQlUUGgtlYPUUW5EnNhcHWqLnLTqDBBCRDjUgy8G/2vwKlEhTPTm4JmDJ7/7g39g8q4kQ2QxvHPy+/r6mFlrDYaQQkqZyWTecVpU8xH9GyUIB6/5W159fwMV+UjetxhjlZIRopVS6t1KAyBCvYpIIwYfrQ+D/AYezUfyvkUpOXRZHdSGoavFIFDWh0o/3iEfqcgHJREoXtSCpbUe1I9o8x3cRKJlhpkdx/ngigtPRz7aaH57MqgBQzediClxqNJ8GGToRvO/2Ku9L5WIVUwAAAAASUVORK5CYII=",
		  					alignment: 'right',
		  					width: 100,
		      				height: 50
		  					},
		  				],
		  			},
		  			{ text: 'N°'+report_nro+'/'+y, fontSize: 12, alignment: 'center', color: '#034694', margin: [0, 5, 0, 8]},
		  			{
		  				table: {
			        		// headers are automatically repeated if the table spans over multiple pages
			        		// you can declare how many rows should be treated as headers
					        headerRows: 1,
					        widths: [ '*', '*'],

					        body: [
					        	[ 	{ text: 'Cliente', fillColor: fillColor_tab1, color: 'white', bold: true, fontSize: 11 }, 
					        		{ text: name, fontSize:11 }  ],
					        	[ 	{ text: 'Lugar', fillColor: fillColor_tab1, color: 'white', bold: true, fontSize: 11 }, 
					        		{ text: place, fontSize: 11 } ],
					        	[ 	{ text: 'Total cebos', fillColor: fillColor_tab1, color: 'white', bold: true, fontSize: 11 }, 
					        		{ text: bait_num, fontSize: 11} ],
					        	[ 	{ text: 'Fecha de control', fillColor: fillColor_tab1, color: 'white', bold: true, fontSize: 11 }, 
					        		{ text: control_date, fontSize: 11} ],
					        	[ 	{ text: 'N° de registro', fillColor: fillColor_tab1, color: 'white', bold: true, fontSize: 11 }, 
					        		{ text: reg_num, fontSize: 11 } ],
					        ]
				    	}
		  			},
		  			{ text: 'Intensidad de presencia', bold: true, fontSize: 10, margin: [0, 8, 0, 3], alignment: 'center' },
					{ text: 'Alta: > 80% cebos con V y P', alignment: 'center', fontSize: 9, margin: [0, 0, 0, 2] },
					{ text: 'Media: 40 - 80% cebos con V y P', alignment: 'center', fontSize: 9, margin: [0, 0, 0, 2] },
					{ text: 'Baja: < 40% cebos con V y P', alignment: 'center', fontSize: 9, margin: [0, 0, 0, 10] },
					{
						table: {
							headerRows: 1,
							widths: colCount,
							body: col,
						}
					},
					{
						image: chartImg,
						width: 450,
						margin: [0, 15, 0, 15]
					},
					{ 
						columns: [ 
							{ text: 'Observaciones:', alignment: 'left', bold: true, fontSize: 10, margin: [0, 10, 0, 0]},
							{ text: obs, alignment: 'left', fontSize: 10, margin: [-140, 10, 0, 0] }
						]
					},
					{
						columns: [
							{ text: 'Medidas Correctivas:', alignment: 'left', bold: true, fontSize: 10, margin: [0, 5, 0, 0]},
							{ text: corr_mess, alignment: 'left', fontSize: 10, margin: [-140, 5, 0, 0] }
						]
					},
					{ 
						text: '_________________________________________________________________________________', 
						alignment: 'center', 
						fontSize: 14 
					},
					{
						text: 'Ecoquality LTDA.',
						fontSize: 8,
						color: '#034694',
						alignment: 'center',
						bold: true,
						margin: [0, 5, 0, 5]
					},
					{
						columns: [
							{
								text: 'Fono: 75-2203900',
								alignment: 'left',
								fontSize: 7
							},
							{
								text: 'Resolución Sanitaria SSM N° 1574/09.07.02',
								fontSize: 7,
								alignment: 'center'
							},
							{
								text: 'Email: mfuentes@ecoquality.cl',
								alignment: 'right',
								fontSize: 7
							}
						]
					}

		  			],
		  			styles: {
		  				header: {
		  					bold: true
		  				},
		  			}
		  		};

		  		var colCount = new Array();

		  		//5 columns
		  		$('#table-test thead').find('th').each(function(index, value){
		  			if(index == 1)
		  			{
		  				colCount.push('40%');
		  			}

		  			colCount.push('15%');
		  		});
		  		
		  		var col = new Array();
		  		col.push([
		  			{ text: 'Tipo', fillColor: fillColor_tab1, color: 'white', fontSize: 11 },
		  			{ text: 'Resultados', fillColor: fillColor_tab1, color: 'white', fontSize: 11 },
		  			{ text: 'Cantidad', fillColor: fillColor_tab1, color: 'white', fontSize: 11 },
		  			{ text: '% Presente', fillColor: fillColor_tab1, color: 'white', fontSize: 11 },
		  			{ text: '% Acumulado', fillColor: fillColor_tab1, color: 'white', fontSize: 11 }
		  		]);

		  		var row = new Array();
		  		$('#table-test tbody').find('tr').each(function(){
		  			
		  			$.each($(this).children(), function(){
						row.push($(this).text());
		  			});

		  			col.push(row);
		  			row = new Array();
		  		});

		  		docDefinition.content[7].table.widths = colCount;
		  		docDefinition.content[7].table.body = col;

		  		
		  	}

	  		$('#btn_download').click(function(){
		  			pdfMake.createPdf(docDefinition).download('Reporte.pdf');
		  	});
        }

  	</script>
@endsection