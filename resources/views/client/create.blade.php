@extends('layout.app')
@section('style')
<style type="text/css" media="screen">
	.preview-img{
	    weight: 500px;
	    height: 362px;
	}

	img {
	    max-width: 100%;
	    max-height: 100%;
	}

	img.center {
	    display: block;
	    margin: 0 auto;
	}
</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		Crear
		<small>Cliente</small>	
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(['action' => 'ClientsController@store', 'files' => true]) }}				
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						{{ Form::label('name', 'Nombre') }}
						{{ Form::text('name','', ['class' => 'form-control', 'required' => '']) }}
						<br>
						{{ Form::label('physical_place', 'Lugar Físico') }}
						{{ Form::text('physical_place','', ['class' => 'form-control', 'required' => '']) }}
						<br>
						{{ Form::label('email', 'Correo electrónico') }}
						{{ Form::email('email','', ['class' => 'form-control', 'required' => '']) }}
						<br>
						{{ Form::label('url_plan_location', 'Plano del lugar') }}
						{{ Form::file('url_plan_location', ['id' => 'plan_input', 'required' => '']) }}
					</div>
					<div class="col-xs-12 col-md-6">
						<p>Vista previa del plano</p>												
						<div class="preview-img">
							<img id="plan_preview" class="center" src="{{ asset('img/no_image.jpg') }}"/>
						</div>									
					</div>
				</div>				
	   		</div>
	   		<div class="box-footer">
				<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>	
				{{ Form::button('<i class="fa fa-plus"></i> Crear', ['type' => 'submit', 'class' => 'btn btn-success pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>
@endsection
@section('script')
<script>
	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#plan_preview').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#plan_input").change(function(){	    
	    var ext = $('#plan_input').val().split('.').pop().toLowerCase();		
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		    alert('invalid extension!');
		}
		else
		{
			readURL(this);
		}
	});
</script>
@endsection