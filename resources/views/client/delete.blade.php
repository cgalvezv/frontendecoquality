<div class="modal modal-danger fade" id="deleteClient">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        {{ Form::open(['method' => 'delete', 'route' => 'client.delete', 'class' => 'form']) }}
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Eliminar cliente</h4>
          </div>
          <div class="modal-body">
            <p id="bodyDeleteModal"></p>
            {!! Form::hidden('id', '', ['id' => 'clientID']) !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-outline">Eliminar</button>
          </div>
        {{ Form::close() }}
    </div>
  </div>
</div>