@extends('layout.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Clientes
		<div class="pull-right">
			<a href="/client/create" class="btn btn-success"><i class="fa fa-users"></i> Crear Cliente</a>
		</div>		
	</h1>
</section>
<section class="content">
	<div class="box box-primary">		
		<div class="box-body">
			<table id="dataTable" class="table table-bordered table-striped">
				<thead>
					<tr>						
						<th>Nombre Cliente</th>
						<th>E-Mail</th>
						<th>Lugar Físico</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($clients as $client)
						<tr>							
							<td>{{ $client->name }}</td>
							<td>{{ $client->email }}</td>
							<td>{{ $client->physical_place }}</td>
							<td>
								<a href="/client/{{ $client->id }}" class="btn btn-default"><i class="fa fa-eye"></i></a>
								<a href="/client/{{ $client->id }}/edit" class="btn btn-warning"><i class="fa fa-edit"></i></a>	
								<button 
									class="btn btn-danger" 
									data-toggle="modal" 									
									data-id="{{ $client->id }}" 
									data-title="¿Realmente desea eliminar al cliente {{ $client->name }} - {{ $client->physical_place }} del sistema?" 
									data-target="#deleteClient" 
								>
									<i class="fa fa-trash"></i>
								</button>		
							</td>	
						</tr>
					@endforeach
				</tbody>
			</table>  		    		
   		</div>
	</div>
</section>
@include('client.delete')
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- Inicializar Datatable-->
<script>	
	$(document).ready(function(){		
	    $('#dataTable').DataTable(
	    {
	    	"language":{
	    		"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
	    	}
	    });
	});
</script>
<script>
	$(function() {
    $('#deleteClient').on("show.bs.modal", function (e) {
         $("#bodyDeleteModal").html($(e.relatedTarget).data('title'));
         $("#clientID").val($(e.relatedTarget).data('id'));
    });
});
</script>
@endsection