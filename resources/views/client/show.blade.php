@extends('layout.app')
@section('style')
<style type="text/css" media="screen">
	.preview-img{
	    weight: 242px;
	    height: 200px;
	}

	img {
	    max-width: 100%;
	    max-height: 100%;
	}

	img.center {
	    display: block;
	    margin: 0 auto;
	}
</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		Detalle
		<small>Cliente</small>	
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<strong>
							<i class="fa fa-users margin-r-5"></i>
							Nombre
						</strong>
						<h4 class="text-muted">{{ $client->name }}</h4>
						<br>
						<strong>
							<i class="fa fa-map-marker margin-r-5"></i>
							Lugar Físico
						</strong>
						<h4 class="text-muted">{{ $client->physical_place }}</h4>
						<br>
					</div>	
					<div class="col-xs-12 col-md-4">
						<strong>
							<i class="fa fa-map-o margin-r-5"></i>
							Plano
						</strong>
						<br>												
						<div class="preview-img">
							<img id="plan_preview" class="center thumbnail" src="{{ Illuminate\Support\Facades\Storage::url($client->url_plan_location) }}"/>
						</div>									
					</div>
					<div class="col-xs-12 col-md-4">
						<strong>
							<i class="fa fa-envelope margin-r-5"></i>
							Correo electrónico
						</strong>
						<h4 class="text-muted">{{ $client->email }}</h4>
					</div>					
				</div>
			</div>		
		</div>
		<div class="box-footer">
			<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>				
		</div>
	</div>
</section>
@endsection
@section('script')

@endsection