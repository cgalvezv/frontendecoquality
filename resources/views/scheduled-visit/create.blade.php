@extends('layout.app')
@section('style')
<!--Datatime Picker Styles-->
<link rel="stylesheet" href="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker-standalone.css') }}">
<!-- Selec2 Styles -->
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
<!-- Boostrap-Toogle Styles -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Crear
		<small>Visita Agendada</small>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(array('action' => 'ScheduledVisitsController@store')) }}
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label>Cliente</label>					
							<select id="select-client" class="form-control" style="width: 100%;"></select>
							{{ Form::hidden('client_id', '',['id' => 'client_id'] ) }}								
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							{{ Form::label('visit_date', 'Fecha de la visita') }}					
			                <div class='input-group date' id='visit-date'>
			                	<span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                    {{Form::text('visit_date',"", ['class' => 'form-control'])}}			                    
			                </div>
			            </div>																		
					</div>								
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							<label for="">Información trampas</label>
							<div class="row">
								<div class="col-md-6 col-xs-6">
									<div class="small-box bg-teal">
							            <div class="inner">
							              	<h3 id="num-cebo">0</h3>
							              	<p>Total de cebos</p>
							            </div>
							            <div class="icon">
							              	<i class="fa fa-warning"></i>
							            </div>
							        </div>                        
								</div>
								<div class="col-md-6 col-xs-6">
									<div class="small-box bg-yellow">
							            <div class="inner">
							              	<h3 id="num-tcv">0</h3>
							              	<p>Total de trampas captura viva</p>
							            </div>
							            <div class="icon">
							              	<i class="fa fa-trash"></i>
							            </div>							          
							        </div>                         
								</div>
							</div>
						</div>		
					</div>		
					<div class="col-xs-12 col-md-6">					
						<div class="form-group">
							<label>Tipo(s) de Plantilla(s)</label>
							<div class="form-group">
								<label class="checkbox">									
								  	<input id="value-cebo" name="value_cebo" checked data-toggle="toggle" data-size="mini" data-on="Sí" data-off="No" type="checkbox">
								  	&emsp;{{ $controlTemplateType[0]['name'] }}
								</label>
								<label class="checkbox">									
								  	<input id="value-capturaviva" name="value_capturaviva" checked data-toggle="toggle" data-size="mini" data-on="Sí" data-off="No" type="checkbox">
								  	&emsp;{{ $controlTemplateType[1]['name'] }}
								</label>
							</div>		
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="form-group">
							<label>Escriba el nombre del monitor que desea agregar</label>					
							<select class="select-monitors form-control" name="user_selected[]" style="width: 100%;" multiple="multiple"></select>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>				
				{{ Form::button('<i class="fa fa-calendar-plus-o"></i> Agendar', ['type' => 'submit', 'class' => 'btn btn-success pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>
@endsection
@section('script')
	<!-- Moment  -->
	<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('plugins/moment/es.js') }}"></script>
	<script src="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<!-- Boostrap-Toogle-->
	<script src="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
	<!--Main Scripts-->
	<script type="text/javascript">
        $(function () {
            $('#visit-date').datetimepicker({ 
            	format: 'DD-MM-YYYY HH:mm ',
                sideBySide: true,
            	locale: 'es'        	
            });

            $('input[id^="value-"]').bootstrapToggle({
            	on: 'Sí',
      			off: 'No'
            });
        });
    </script>
	<!-- Select2 -->
	<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
	<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
	<!-- Selectores -->
	<script>
		$('#select-client').select2({
			language: 'es',
			placeholder: 'Seleccione cliente',
			ajax: {
			  	url: '/client/search',
			  	dataType: 'json',
			  	delay: 250,
			 	data: function (params) {
			        return {
			            term: params.term
			        };
			    },
			    processResults: function (data, params) 
			    { 
			        return {
			          	results: $.map(data, function (item) {
			              	return {
			                  	text: item.full_name,
			                  	id: item.id
			              	}
			          	})
			     	 };
			    },
			  	cache: true,
			},
		});
		$('.select-monitors').select2({
			language: 'es',
			placeholder: 'Seleccione el(los) monitor(es) para realizar esta plantilla',
			ajax: {
			  url: '/visits-users/search',
			  dataType: 'json',
			  delay: 250,
			  data: function (params) {		  		
			      	return {		      		
			        	term: params.term
			      	};
			  },
			  processResults: function (data, params) {
			      return {
			          results: $.map(data, function (item) {
			              return {
			                  text: item.name,
			                  id: item.rut
			              }
			          })
			      };
			  },
			  cache: true,
			},
		});		
	</script>	
	<!-- Obtener plantillas de control e info trampas -->
	<script>
		var clientSelected = 0;

		$("#select-client").change(function(){		

			clientSelected = $(this).val();	
			$('input[id^="value-"]').bootstrapToggle('enable');
			$('input[id^="value-"]').bootstrapToggle('on');		

			if(clientSelected > 0 )
			{
				$.ajax({
					url: '/control-template/get',
	                data: { 
	                	id_client: clientSelected
	                },
	                type: "GET",
	                dataType: "json",
	                success: function (result) {           		                	
	                	if(result.length > 1) {
	                	 	for (var i = 0; i < result.length; i++) {
	                	 		if (result[i].id_control_template_type == 1) {
	                	 			$("#num-cebo").text(result[i].total_trap);
	                	 		} 
	                	 		else {
	                	 			$("#num-tcv").text(result[i].total_trap);
	                	 		}
	                	 	}
	                	}
	                	else {
	                		if(result[0].id_control_template_type == 1) {	                		
	                			$('#value-capturaviva').bootstrapToggle('off');
	                			$('#value-capturaviva').bootstrapToggle('disable');
	                			$("#num-tcv").text("0");
	                			$("#num-cebo").text(result[0].total_trap);
	                		}
	                		else {
	                			$('#value-cebo').bootstrapToggle('off');
	                			$('#value-cebo').bootstrapToggle('disable');
	                			$("#num-tcv").text(result[0].total_trap);	
	                			$("#num-cebo").text("0");                			
	                		}
	                	}
	                	$("#client_id").val(clientSelected);
	                }
				});
			}			
		});	
	</script>
@endsection