@extends('layout.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.min.css') }}">
<!-- Selec2 Styles -->
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Visitas Agendadas
		<small>Lista</small>
		<div class="pull-right">
			<a href="/scheduled-visit/calendar" class="btn btn-default"><i class="fa fa-calendar"></i> Ir a Calendario</a>
			<a href="/scheduled-visit/create" class="btn btn-success"><i class="fa fa-calendar-plus-o"></i> Crear Visita</a>
		</div>		
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">				
		<div class="box-body">
			<table id="dataTable" class="table table-bordered table-striped">
				<thead>
					<tr>						
						<th>Cliente - Lugar</th>						
						<th>Tipo De Plantilla</th>
						<th>Fecha Revisión</th>
						<th>Monitores</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($response as $visit)
						@if($visit['is_visited'] == 0)
							<tr>							
								<td>{{ $visit['client_name'] }} - {{ $visit['client_physical_place'] }}</td>
								<td>{{ $visit['control_template_type'] }}</td>
								<td>{{ $visit['visit_date']->diffForHumans() }}</td>
								<td width="25%">
									@foreach($visit['monitors'] as $monitor)
										<span class="label label-primary">{{ $monitor }}</span>
									@endforeach
								</td>
								<td>
									<a href="/scheduled-visit/{{ $visit['id'] }}" class="btn btn-default"><i class="fa fa-eye"></i> </a>
									<a href="/scheduled-visit/{{ $visit['id'] }}/edit" class="btn btn-warning"><i class="fa fa-edit"></i> </a>		
									<button 
										class="btn btn-danger" 
										data-toggle="modal" 									
										data-id="{{ $visit['id'] }}" 
										data-title="Visita del día {{ $visit['visit_date']->format('d/m/Y') }} a las {{ $visit['visit_date']->format('H:i') }}, para el cliente {{ $visit['client_name'] }} - {{ $visit['client_physical_place'] }}, correspondiente al tipo {{ $visit['control_template_type'] }}" 
										data-target="#deleteScheduledVisit" 
									>
										<i class="fa fa-trash"></i>
									</button>											
								</td>	
							</tr>
						@endif
					@endforeach
				</tbody>
			</table>   		    		
   		</div>
	</div>
</section>
@include('scheduled-visit.delete')
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- Inicializar Datatable-->
<script>	
	$(document).ready(function(){		
	    $('#dataTable').DataTable(
	    {
	    	"language":{
	    		"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
	    	},
	    	"order": [[ 2, "asc" ]]
	    });
	});
</script>
<script>
	$(function() {
    $('#deleteScheduledVisit').on("show.bs.modal", function (e) {
         $("#bodyDeleteModal").html($(e.relatedTarget).data('title'));
         $("#visitID").val($(e.relatedTarget).data('id'));
    });
});
</script>
@endsection