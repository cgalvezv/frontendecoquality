@extends('layout.app')
@section('style')
<!--Datatime Picker Styles-->
<link rel="stylesheet" href="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker-standalone.css') }}">
<!-- Selec2 Styles -->
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
<!-- Boostrap-Toogle Styles -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Editar
		<small>Visita Agendada</small>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(['method' => 'put', 'route' => ['scheduled-visit.update', $data['id']] , 'class' => 'form']) }}
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-7">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="form-group">
									<label>Cliente</label>					
									<select id="select-client" class="form-control" style="width: 100%;"></select>	
									{{ Form::hidden('client_id', $data['client_id'],['id' => 'client_id'] ) }}								
								</div>
							</div>													
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="form-group">
									{{ Form::label('visit_date', 'Fecha de la visita') }}					
					                <div class='input-group date' id='visit-date'>
					                	<span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                    {{Form::text('visit_date',$data['visit_date'], ['class' => 'form-control'])}}			                    
					                </div>
					            </div>																		
							</div>								
						</div>
					</div>
					<div class="col-xs-12 col-md-5">
						<div class="form-group">
							<label for="">Información trampas</label>
							<div class="row">
								@if($data['type_trap'] == 1)
								<div class="col-md-12 col-xs-12">
									<div class="small-box bg-teal">
							            <div class="inner">
							              	<h3 id="num-cebo">0</h3>
							              	<p>Total de cebos</p>
							            </div>
							            <div class="icon">
							              	<i class="fa fa-warning"></i>
							            </div>
							        </div>                        
								</div>
								@else
									<div class="col-md-12 col-xs-12">
										<div class="small-box bg-yellow">
								            <div class="inner">
								              	<h3 id="num-tcv">0</h3>
								              	<p>Total de trampas captura viva</p>
								            </div>
								            <div class="icon">
								              	<i class="fa fa-trash"></i>
								            </div>							          
								        </div>                         
									</div>
								@endif
								{{ Form::hidden('trap_type', $data['type_trap']) }}
							</div>
						</div>		
					</div>	
				</div>		
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="form-group">
							<label>Escriba el nombre del monitor que desea agregar</label>					
							<select class="select-monitors form-control" name="user_selected[]" style="width: 100%;" multiple="multiple">
								@foreach($data['monitors'] as $monitor)
									<option value="{{ $monitor['rut'] }}" selected>{{ $monitor['name'] }}</option>
								@endforeach
							</select>							
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>{{ Form::button('<i class="fa fa-refresh"></i> Editar', ['type' => 'submit', 'class' => 'btn btn-warning pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>

@endsection
@section('script')
	<!-- Moment  -->
	<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>	
	<script src="{{ asset('plugins/moment/es.js') }}"></script>
	<script src="{{ asset('plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
	<!-- Boostrap-Toogle-->
	<script src="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
	<!--Main Scripts-->
	<script type="text/javascript">
        $(function () {
            $('#visit-date').datetimepicker({ 
            	format: 'DD-MM-YYYY HH:mm ',
                sideBySide: true,
            	locale: 'es'        	
            });

            $('input[id^="value-"]').bootstrapToggle({
            	on: 'Sí',
      			off: 'No'
            });
        });
    </script>
    	<!-- Select2 -->
	<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
	<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
	<!-- Selectores -->
	<script>
		var idClient = $('#client_id').val();

		function getClients(handleData){
			$.ajax({
				url: '/client/search',
				dataType: 'json',
				delay: 250,
			 	data: term = '',
			    success: function(response) {
			    	var data = [];
			    	$.each(response, function (key, value) {	                 
			    		data.push({
			    			id: value.id,
			    			text: value.full_name
			    		});
		            });
		            handleData(data);
			    }
			});
		};

		function getMonitors(handleData){
			$.ajax({
				url: '/visits-users/search',
				dataType: 'json',
				delay: 250,
			 	data: term = '',
			    success: function(response) {
			    	var data = [];
			    	$.each(response, function (key, value) {	                 
			    		data.push({
			    			id: value.rut,
			    			text: value.name
			    		});
		            });
		            handleData(data);
			    }
			});
		}
				
		getClients(function(output){
			$('#select-client').select2({
				language: 'es',
				placeholder: 'Seleccione cliente',
				data : output
			});

			$('#select-client').val(idClient).trigger("change");	
		});

		getMonitors(function(output){
			$('.select-monitors').select2({
				language: 'es',
				placeholder: 'Seleccione el(los) monitor(es) para realizar esta plantilla',
				data: output
			});	
		});
	</script>
	<!-- Obtener plantillas de control e info trampas -->
	<script>
		var clientSelected = $('#client_id').val();
		
		if(clientSelected > 0 )
		{											
			getTotalTrap(clientSelected);
		}

		$("#select-client").change(function(){
			if(clientSelected > 0 )
			{											
				clientSelected = $(this).val();
				getTotalTrap(clientSelected);	
			}
		});		

		function getTotalTrap(clientSelected) {
			$.ajax({
				url: '/control-template/get',
                data: { 
                	id_client: clientSelected
                },
                type: "GET",
                dataType: "json",
                success: function (result) {           		                	
                	if(result.length > 1) {
                	 	for (var i = 0; i < result.length; i++) {
                	 		if (result[i].id_control_template_type == 1) {
                	 			$("#num-cebo").text(result[i].total_trap);
                	 		} 
                	 		else {
                	 			$("#num-tcv").text(result[i].total_trap);
                	 		}
                	 	}
                	}
                	else {
                		if (result[0].id_control_template_type == 1) {
                	 			$("#num-cebo").text('0');
                	 		} 
                	 		else {
                	 			$("#num-tcv").text('0');
                	 		}
                	}
                	$("#client_id").val(clientSelected);
                }
			});
		}
	</script>	
@endsection