@extends('layout.app')
@section('style')
<style type="text/css" media="screen">
	.preview-img{
	    weight: 242px;
	    height: 200px;
	}

	img {
	    max-width: 100%;
	    max-height: 100%;
	}

	img.center {
	    display: block;
	    margin: 0 auto;
	}
</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		Detalle
		<small>Visitas Agendadas</small>
	</h1>
</section>
<section class="content">
	<div class="row">
			<div class="col-md-3 col-xs-12">
			<div class="info-box">
	            <span class="info-box-icon bg-blue"><i class="fa fa-calendar-o"></i></span>
	            <div class="info-box-content">
	              <span class="info-box-text">Fecha de visita</span>
	              <span class="info-box-number">{{ $data['visitDate'] }} hrs</span>
	            </div>
	          </div>
		</div>
		<div class="col-md-3 col-xs-12">
			<div class="info-box">
	            <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
	            <div class="info-box-content">
	              <span class="info-box-text">Cliente</span>
	              <span class="info-box-number">{{ $data['client']->name }}</span>
	            </div>
	          </div>
		</div>
		<div class="col-md-3 col-xs-12">
			<div class="info-box">
	            <span class="info-box-icon bg-red"><i class="fa fa-map-marker"></i></span>
	            <div class="info-box-content">
	              <span class="info-box-text">Lugar</span>
	              <span class="info-box-number">{{ $data['client']->physical_place }}</span>
	            </div>
	          </div>
		</div>
		
			<div class="col-md-3 col-xs-12">
			<div class="info-box">
				@if($data['controlTemplate']->id_control_template_type == 1)
	            	<span class="info-box-icon bg-teal"><i class="fa fa-warning"></i></span>
	            @else
	            	<span class="info-box-icon bg-yellow"><i class="fa fa-trash"></i></span>
	            @endif
	            <div class="info-box-content">
	              <span class="info-box-text">Tipo de trampa</span>
	              <span class="info-box-number">{{ $data['trapType'] }}</span>
	            </div>
	          </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="box box-primary">
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<h4>Monitores asociados a la visita</h4>
							<hr>
							<ul class="list-group">
								@foreach($data['monitors'] as $monitor)
									<li class="list-group-item">
										<b>{{ $monitor->name }} {{ $monitor->lastname }}</b>
										<a href="/user/{{ $monitor->rut }}" class="btn btn-xs btn-default pull-right">Ver detalle</a>
									</li>
								@endforeach							  
							</ul>
						</div>
						<div class="col-xs-12 col-md-4">
						 	<h4>Plano del lugar</h4>
							<hr>
						    <div class="preview-img">
								<img id="plan_preview" class="center thumbnail" src="{{ Illuminate\Support\Facades\Storage::url($data['client']->url_plan_location) }}"/>
							</div>
						</div>
						<div class="col-xs-12 col-md-4">
						 	<h4>Otra información</h4>
							<hr>
					     	<dl>
								<dt>Cantidad de trampas</dt>
								<dd>{{ $data['controlTemplate']->total_trap }} trampas</dd>
								<dt>Email del cliente</dt>
								<dd>{{ $data['client']->email }}</dd>
							</dl> 
						</div>
					</div>
		   		</div>
		   		<div class="box-footer">
		   			<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>
		   		</div>
			</div>
		</div>
	</div>	
</section>
@endsection