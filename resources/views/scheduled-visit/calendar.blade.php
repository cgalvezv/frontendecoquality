@extends('layout.app')
@section('style')
<style>
	.calendar-content {
		max-width: 1500px;
		margin: 0 auto;	
	}
</style>
<!-- FullCalendar -->
<link rel="stylesheet" href="{{ asset('plugins/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" media='print' href="{{ asset('plugins/fullcalendar/fullcalendar.print.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Visitas Agendadas
		<small>Calendario</small>
		<div class="pull-right">
			<a href="/scheduled-visit" class="btn btn-default"><i class="fa fa-list"></i> Ir a Lista</a>
			<button type="button" class="btn btn-primary" onclick="printCalendar('print')"><i class="fa fa-print"></i> Imprimir</button>
			<a href="/scheduled-visit/create" class="btn btn-success" ><i class="fa fa-calendar-plus-o"></i> Crear Visita</a>			
		</div>	
	</h1>
</section>
<section class="content">
	<div class="row" id="print">
		<div class="col-md-12 col-xs-12">
			<div class="box box-primary calendar-content">
				<div class="box-body">
		  		 	<div id='calendar'></div>
		   		</div>
			</div>
		</div>
	</div>	
</section>
@endsection
@section('script')
<!-- Moment  -->
<script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
<!-- FullCalendar -->
<script src="{{ asset('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/locale/es.js') }}"></script>
<!-- Initialize FullCalendar-->
<script>	
	$(document).ready(function(){		
	    $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			allDayText: false,
			allDaySlot: false,
			defaultTimedEventDuration: '01:00:00',
			slotLabelFormat: 'h(:mm)a',
			navLinks: true,
			eventLimit: true,
		    events: {
		        url: '/scheduled-visit/dates',
		        type: 'POST',
		        data:{
		        	"_token": "{{ csrf_token() }}"
		        },
		        error: function() {
		            alert('Error al cargar las fechas');
		        },
		        color: '#034694',   
		        textColor: 'white' 
		    }
		});
	});
</script>
<!--Print Calendar-->
<script>
	function printCalendar(divName) {
	     var printContents = document.getElementById(divName).innerHTML;
	     var originalContents = document.body.innerHTML;

	     document.body.innerHTML = printContents;

	     window.print();

	     document.body.innerHTML = originalContents;
	}
</script>
@endsection