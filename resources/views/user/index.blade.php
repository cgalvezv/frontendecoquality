@extends('layout.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Usuarios	
		<div class="pull-right">
			<a href="/user/create" class="btn btn-success"><i class="fa fa-user"></i> Crear Usuario</a>
		</div>		
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">				
		<div class="box-body">
			<table id="dataTable" class="table table-bordered table-striped">
				<thead>
					<tr>						
						<th>Nombre Persona</th>						
						<th>Rol</th>
						<th>Nombre Usuario</th>
						<th>E-Mail</th>						
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $user)
						<tr>
							<td>{{ $user['name_person'] }}</td>
							<td>{{ $user['role'] }}</td>
							<td>{{ $user['username'] }}</td>
							<td>{{ $user['email'] }}</td>							
							<td>
								<a href="/user/{{ $user['rut_person'] }}" class="btn btn-default"><i class="fa fa-eye"></i> </a>
								<a href="/user/{{ $user['id'] }}/edit" class="btn btn-warning"><i class="fa fa-edit"></i> </a>		
								<button 
									class="btn btn-danger" 
									data-toggle="modal" 									
									data-id="{{ $user['id'] }}" 
									data-title="¿Desea eliminar el usuario {{ $user['username'] }}, perteneciente a {{ $user['name_person'] }}, del sistema?" 
									data-target="#deleteUser" 
								>
									<i class="fa fa-trash"></i>
								</button>											
							</td>	
						</tr>
					@endforeach
				</tbody>
			</table>  
   		</div>
	</div>
</section>
@include('user.delete')
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- Inicializar Datatable-->
<script>	
	$(document).ready(function(){		
	    $('#dataTable').DataTable(
	    {
	    	"language":{
	    		"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
	    	}
	    });
	});
</script>
<script>
	$(function() {
	    $('#deleteUser').on("show.bs.modal", function (e) {
	         $("#bodyDeleteModal").html($(e.relatedTarget).data('title'));
	         $("#userID").val($(e.relatedTarget).data('id'));
	    });
	});
</script>
@endsection