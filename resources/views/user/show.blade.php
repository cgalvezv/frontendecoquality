@extends('layout.app')
@section('style')
@endsection
@section('content')
<section class="content-header">
	<h1>
		Detalle
		<small>Usuario</small>	
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<div class="row">
			 	<div class=" col-xs-12 col-md-12">
			 		<center><h2>{{ $person->name }} {{ $person->lastname }}</h2></center>
			 		<center><h4 class="text-muted">{{ \App\UserRole::find($user->id_user_role)->role }}</h4></center>			 		
			 	</div>
			</div>	
			<hr>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<strong>
							<i class="fa fa-user margin-r-5"></i>
							Nombre usuario
						</strong>
						<p class="text-muted">{{ $user->username }}</p>
						<br>
						<strong>
							<i class="fa fa-envelope margin-r-5"></i>
							Correo electrónico
						</strong>
						<p class="text-muted">{{ $user->email }}</p>
					</div>
					<div class="col-xs-12 col-md-4">
						<strong>
							<i class="fa fa-id-card margin-r-5"></i>
							R.U.T
						</strong>
						<input type="hidden" id="rut" value="{{ $user->rut_person }}">
						<p class="text-muted" id="text"></p>												
						<br>
						<strong>
							<i class="fa fa-home margin-r-5"></i>
							Domicilio
						</strong>
						<p class="text-muted">{{ $person->address }}</p>
					</div>
					<div class="col-xs-12 col-md-4">
						<strong>
							<i class="fa fa-phone margin-r-5"></i>
							Nombre usuario
						</strong>
						<p class="text-muted">{{ $person->phone_number }}</p>
						<br>
						<strong>
							<i class="fa fa-calendar margin-r-5"></i>
							Fecha creación
						</strong>
						<p class="text-muted">{{ $person->created_at }}</p>
					</div>
				</div>
			</div>		
		</div>
		<div class="box-footer">
			<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>				
		</div>
	</div>
</section>
@endsection
@section('script')
<script>
	$( document ).ready(function() {
	    $("#text").text($("#rut").val()+"-"+getDV($("#rut").val()));
	});

	function getDV(numero) {
        nuevo_numero = numero.toString().split("").reverse().join("");
        for(i=0,j=2,suma=0; i < nuevo_numero.length; i++, ((j==7) ? j=2 : j++)) {
            suma += (parseInt(nuevo_numero.charAt(i)) * j); 
        }
        n_dv = 11 - (suma % 11);
        return ((n_dv == 11) ? 0 : ((n_dv == 10) ? "K" : n_dv));
    }
</script>
@endsection