@extends('layout.app')
@section('style')
<!-- Selec2 Styles -->
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Crear
		<small>Usuario</small>	
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(array('action' => 'UsersController@store')) }}				
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="form-group">
							{{ Form::label('rut_person', 'Persona') }}												
							<select class="form-control select-person" name="rut_person" style="width: 100%;" required=""></select>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="form-group">
							{{ Form::label('id_user_role', 'Rol') }}												
							<select class="form-control select-role" name="id_user_role" style="width: 100%;" required="">
								@foreach($roles as $role)
									<option value="{{ $role->id }}">{{ $role->role }}</option>									
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="form-group">
							{{ Form::label('email', 'Correo electrónico') }}
							{{ Form::email('email','', ['class' => 'form-control', 'required' => '']) }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							{{ Form::label('username', 'Nombre Usuario') }}
							{{ Form::text('username','', ['class' => 'form-control', 'required' => '']) }}
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group">
							{{ Form::label('password', 'Contraseña') }}
							{{ Form::password('password', ['class' => 'form-control', 'required' => '']) }}
						</div>
					</div>
				</div>
	   		</div>
	   		<div class="box-footer">
				<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>	
				{{ Form::button('<i class="fa fa-plus"></i> Crear', ['type' => 'submit', 'class' => 'btn btn-success pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>
@endsection
@section('script')
<!-- Select2 -->
<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
<script>
	$('.select-person').select2({
		language: 'es',
		placeholder: 'Seleccione la persona para crear el usuario',
		ajax: {
		  url: '/person/search',
		  dataType: 'json',
		  delay: 250,
		  data: function (params) {		  		
		      	return {		      		
		        	term: params.term
		      	};
		  },
		  processResults: function (data, params) {
		      return {
		          results: $.map(data, function (item) {
		              return {
		                  text: item.name,
		                  id: item.rut
		              }
		          })
		      };
		  },
		  cache: true,
		},
	});

	$('.select-role').select2({
		language: 'es',
		placeholder: 'Seleccione el rol para el usuario'
	});
</script>
@endsection