@extends('layout.app')
@section('style')
<!-- Selec2 Styles -->
<link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Crear
		<small>Plantilla de Control</small>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		{{ Form::open(array('action' => 'ControlTemplatesController@store')) }}
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<div class="form-group">
							<label>Cliente</label>					
							<select id="select-client" class="form-control" style="width: 100%;" required=""></select>
							{{ Form::hidden('client_id', '',['id' => 'client_id'] ) }}								
						</div>
					</div>
					<div class="col-xs-12 col-md-4">
						<div class="form-group">
							<label>Tipo de plantilla</label>					
							<select id="select-template" name="template_id" class="form-control" style="width: 100%;">
								<option value="1">{{ $controlTemplateType[0]['name'] }}</option>
								<option value="2">{{ $controlTemplateType[1]['name'] }}</option>
							</select>							
						</div>
					</div>	
					<div class="col-xs-12 col-md-4">
						{{ Form::label('total_trap', 'Total de trampas') }}
						{{ Form::number('total_trap',1, ['id' => 'total_trap_number', 'class' => 'form-control', 'required' => '' , 'min' => '1']) }}
					</div>
				</div>
				<hr>
				<div class="row">					
					<div class="col-xs-12 col-md-12">
						<h4>
							Trampas asociadas
						</h4>												
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<p>Acá podrá asignar las localizaciones de las trampas asignadas a la plantilla de control por crear.</p>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<label for="north_values"><span class="label label-primary">TRAMPAS ZONA NORTE</span></label>
										<input type="text" name="north_values" class="form-control" placeholder="Por ejemplo: 1-6,13,15-29" pattern="[^a-zA-Z!@#$%*.?]+" required="">
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<label for="south_values"><span class="label label-primary">TRAMPAS ZONA SUR</span></label>
										<input type="text" name="south_values" class="form-control" placeholder="Por ejemplo: 1-6,13,15-29" pattern="[^a-zA-Z!@#$%*.?]+" required="">
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<label for="east_values"><span class="label label-primary">TRAMPAS ZONA ESTE</span></label>
										<input type="text" name="east_values" class="form-control" placeholder="Por ejemplo: 1-6,13,15-29" pattern="[^a-zA-Z!@#$%*.?]+" required="">
									</div>
								</div>
								<br>
								<br>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<label for="west_values"><span class="label label-primary">TRAMPAS ZONA OESTE</span></label>
										<input type="text" name="west_values" class="form-control" placeholder="Por ejemplo: 1-6,13,15-29" pattern="[^a-zA-Z!@#$%*.?]+" required="">
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-6">
								<div class="well">
									<h5><b>INDICACIONES PARA LA ASOCIACIÓN DE TRAMPAS</b></h5>
									<ul>
										<li><b>Para el rango de trampas:</b> Debe escribir entre el inicio y el fin del rango, un "-". <p>Por ejemplo: Si desea asociar en un sector, las trampas desde la 1 a la 10, debe colocar 1-10.</p></li>
										<li><b>Para las trampas separadas:</b> Debe escribir entre un número de trampa y otro, una ",". <p>Por ejemplo: Si desea asociar en un sector, las trampas 1,6 y 8, debe colocar 1,6,8.</p></li>
										<li>Cabe destacar que estas dos reglas pueden ser combinadas entre sí. <p>Por ejemplo: Para asociar en un sector, las trampas desde el 1 al 10, y aparte la 12 y la 15, se debe escribir 1-10,12,15.</p></li>
										<li>Si en un sector no hay trampas asociadas, se debe escribir un <b>0</b>.</li>
										<li><b>OBSERVACIÓN:</b> Para efectos de asociación de trampas, al momento de asociar las trampas de captura viva, se debe suponer que los identificadores de las trampas son iguales que los cebos (solo números). Por ejemplo: Para asociar las trampas desde la A-1 hasta las A-10, se debe escribir 1-10, y así, de manera consecutiva (esto es solo para efectos de asociación de trampas, en base de datos se guarda como corresponde).</li>								
									</ul>
								</div>
							</div>
						</div>				
					</div>
				</div>
			</div>
			<br>
			<div class="box-footer">
				<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>				
				{{ Form::button('<i class="fa fa-plus"></i> Crear', ['type' => 'submit', 'class' => 'btn btn-success pull-right'] )  }}
			</div>
		{{ Form::close() }}
	</div>
</section>
@endsection
@section('script')
	<!-- Select2 -->
	<script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
	<script src="{{ asset('plugins/select2/i18n/es.js') }}"></script>
	<!-- Selectores -->
	<script>
		$('#select-client').select2({
			language: 'es',
			placeholder: 'Seleccione cliente',
			ajax: {
			  	url: '/client/search',
			  	dataType: 'json',
			  	delay: 250,
			 	data: function (params) {
			        return {
			            term: params.term
			        };
			    },
			    processResults: function (data, params) 
			    { 
			        return {
			          	results: $.map(data, function (item) {
			              	return {
			                  	text: item.full_name,
			                  	id: item.id
			              	}
			          	})
			     	 };
			    },
			  	cache: true,
			},
		});	

		$('#select-template').select2({
			language: 'es',
			placeholder: 'Seleccione tipo de plantilla'
		});
	</script>
	<script>
		var clientSelected = 0;

		$("#select-client").change(function(){		
			clientSelected = $(this).val();
			$("#client_id").val(clientSelected);
		});
	</script>
@endsection