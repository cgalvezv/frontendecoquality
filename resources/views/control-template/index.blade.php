@extends('layout.app')
@section('style')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.min.css') }}">
@endsection
@section('content')
<section class="content-header">
	<h1>
		Plantillas De Control		
		<div class="pull-right">
			<a href="/control-template/create" class="btn btn-success"><i class="fa fa-clipboard"></i> Crear Plantilla de Control</a>
		</div>		
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">				
		<div class="box-body">
			<table id="dataTable" class="table table-bordered table-striped">
				<thead>
					<tr>						
						<th>Cliente - Lugar</th>						
						<th>Tipo De Plantilla</th>
						<th>Total de trampas</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $template)
						<tr>							
							<td>{{ $template['client_name'] }} - {{ $template['client_physical_place'] }}</td>
							<td>{{ $template['control_template_type'] }}</td>
							<td>{{ $template['total_trap'] }}</td>
							<td>
								<a href="/control-template/{{ $template['id'] }}" class="btn btn-default"><i class="fa fa-eye"></i> </a>	
								<button 
									class="btn btn-danger" 
									data-toggle="modal" 									
									data-id="{{ $template['id'] }}" 
									data-title="¿Realmente desea eliminar la plantilla de control correspondiente a {{ $template['client_name'] }} - {{ $template['client_physical_place'] }}, del tipo {{ $template['control_template_type'] }}, con un total de {{ $template['total_trap'] }} trampas?" 
									data-target="#deleteControlTemplate" 
								>
									<i class="fa fa-trash"></i>
								</button>											
							</td>	
						</tr>
					@endforeach
				</tbody>
			</table>  
   		</div>
	</div>
</section>
@include('control-template.delete')
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- Inicializar Datatable-->
<script>	
	$(document).ready(function(){		
	    $('#dataTable').DataTable(
	    {
	    	"language":{
	    		"url" : "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
	    	}
	    });
	});
</script>
<script>
	$(function() {
    $('#deleteControlTemplate').on("show.bs.modal", function (e) {
         $("#bodyDeleteModal").html($(e.relatedTarget).data('title'));
         $("#templateID").val($(e.relatedTarget).data('id'));
    });
});
</script>
@endsection