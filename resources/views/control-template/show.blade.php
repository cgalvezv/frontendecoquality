@extends('layout.app')
@section('style')
@endsection
@section('content')
<section class="content-header">
	<h1>
		Detalle
		<small>Plantilla de control</small>	
	</h1>	
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					
				</div>
				<div class="col-xs-12 col-md-6">
					<strong>
						<i class="fa fa-users margin-r-5"></i>
						Trampas
					</strong>
					<hr>
					<p>Zona Norte</p>
					@foreach($north_traps as $trap)
						<span class="label label-primary">{{ $trap }}</span>
					@endforeach
					<br>
					<p>Zona Sur</p>
					@foreach($south_traps as $trap)
						<span class="label label-primary">{{ $trap }}</span>
					@endforeach
					<br>
					<p>Zona Este</p>
					@foreach($east_traps as $trap)
						<span class="label label-primary">{{ $trap }}</span>
					@endforeach
					<br>
					<p>Zona Oeste</p>
					@foreach($west_traps as $trap)
						<span class="label label-primary">{{ $trap }}</span>
					@endforeach
					<br>
				</div>
			</div>					
		</div>
		<div class="box-footer">
			<button class="btn btn-default pull-left" onclick="window.history.back()"><i class="fa fa-arrow-left"></i> Volver</button>				
		</div>
	</div>
</section>
@endsection
@section('script')
@endsection