<!DOCTYPE html>
<html>
@include('partials.styles')
<head>
  <meta charset="UTF-8">
</head>
<style type="text/css" media="screen">
  img.center 
  {
      display: block;
      margin: 0 auto;
  }
</style>
<body class="hold-transition login-page">
  <div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Servicio</b>Desratización</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
      <img src="http://www.ecoquality.cl/sistema/service/img/logo_ecoquality.png" class="center">
      <br>
      <br>
      <p class="login-box-msg">Inicie sesión para ingresar al sistema.</p>

      <form method="post" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="form-group has-feedback">
          <input name="email" type="email" class="form-control" placeholder="Correo electrónico">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input name="password" type="password" class="form-control" placeholder="Contraseña">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">          
          <div class="col-xs-12 col-md-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar Sesión</button>
          </div>
        </div>
      </form>
      <br>
      <center><a href="{{ route('password.request') }}">¿Has olvidado tu contraseña?</a></center>            
  </div>
</div>
@include('partials.scripts')
</body>
</html>
