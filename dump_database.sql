INSERT INTO `trap_locations` (`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Norte","N", NOW(), NOW());
INSERT INTO `trap_locations` (`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Sur","S", NOW(), NOW());
INSERT INTO `trap_locations` (`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Este","E", NOW(), NOW());
INSERT INTO `trap_locations` (`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Oeste","O", NOW(), NOW());

INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Vacío","V", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Parcialmente comido","P", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Desaparecido","D", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Sin novedad","S", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Comido por otra especie","C", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Sin novedad","S", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Captura","C", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Extraviada","E", NOW(), NOW());
INSERT INTO `trap_states`(`name`,`abbreviation`,`created_at`,`updated_at`) VALUES ("Dañada","D", NOW(), NOW());

INSERT INTO `trap_types` (`name`,`created_at`,`updated_at`) VALUES ("Cebo", NOW(), NOW());
INSERT INTO `trap_types` (`name`,`created_at`,`updated_at`) VALUES ("Trampa de captura viva", NOW(), NOW());

INSERT INTO `control_template_types` (`name`,`created_at`,`updated_at`) VALUES ("Plantilla Verde Cebo", NOW(), NOW());
INSERT INTO `control_template_types` (`name`,`created_at`,`updated_at`) VALUES ("Plantilla Amarilla TCV", NOW(), NOW());

INSERT INTO `clients` (`name`, `email`,`physical_place`,`url_plan_location`,`created_at`,`updated_at`) VALUES ("Viña Aresti", "", "Planta Bellavista", "", NOW(), NOW());
INSERT INTO `clients` (`name`, `email`,`physical_place`,`url_plan_location`,`created_at`,`updated_at`) VALUES ("Viña Montes", "", "Planta Apalta", "", NOW(), NOW());
INSERT INTO `clients` (`name`, `email`,`physical_place`,`url_plan_location`,`created_at`,`updated_at`) VALUES ("Viña Montes", "", "Planta Chimbarongo", "", NOW(), NOW());
INSERT INTO `clients` (`name`, `email`,`physical_place`,`url_plan_location`,`created_at`,`updated_at`) VALUES ("Viña Montes", "", "Planta Marchigüe", "", NOW(), NOW());
INSERT INTO `clients` (`name`, `email`,`physical_place`,`url_plan_location`,`created_at`,`updated_at`) VALUES ("Inacap", "", "Sede Curicó", "", NOW(), NOW());
INSERT INTO `clients` (`name`, `email`,`physical_place`,`url_plan_location`,`created_at`,`updated_at`) VALUES ("Abastible San Fernando", "", "San Fernando", "", NOW(), NOW());

INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (1,1,200,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (1,2,16,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (2,1,210,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (2,2,17,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (3,1,220,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (3,2,18,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (4,1,230,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (4,2,19,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (5,1,240,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (5,2,11,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (6,1,250,NOW(),NOW());
INSERT INTO `control_templates` (`id_client`, `id_control_template_type`, `total_trap`, `created_at`, `updated_at`) VALUES (6,2,12,NOW(),NOW());

INSERT INTO `persons`(`rut`,`name`,`lastname`,`address`,`phone_number`,`created_at`,`updated_at`) VALUES ("17746712","Camilo","Gálvez Vidal","Población Mataquito, Pasaje 5, Casa #101, Curicó","+5699999999",NOW(),NOW());
INSERT INTO `persons`(`rut`,`name`,`lastname`,`address`,`phone_number`,`created_at`,`updated_at`) VALUES ("16889493","Halyl","Zett Lobos","Psje. Cabildo #1385 sector la marqueza, Curicó","+5699999999",NOW(),NOW());
INSERT INTO `persons`(`rut`,`name`,`lastname`,`address`,`phone_number`,`created_at`,`updated_at`) VALUES ("18344216","Luis","Torres González","Población Mataquito, Pasaje 5, Casa #101","+5699999999",NOW(),NOW());
INSERT INTO `persons`(`rut`,`name`,`lastname`,`address`,`phone_number`,`created_at`,`updated_at`) VALUES ("9100421","Omar","Gálvez Vilchez","Millahue de Apalta S/N","+5699999999",NOW(),NOW());
INSERT INTO `persons`(`rut`,`name`,`lastname`,`address`,`phone_number`,`created_at`,`updated_at`) VALUES ("7915421","María","Vidal Morales","Millahue de Apalta S/N","+5699999999",NOW(),NOW());

INSERT INTO `user_roles`(`role`,`created_at`,`updated_at`) VALUES ("Administrador",NOW(),NOW());
INSERT INTO `user_roles`(`role`,`created_at`,`updated_at`) VALUES ("Gerente",NOW(),NOW());
INSERT INTO `user_roles`(`role`,`created_at`,`updated_at`) VALUES ("Monitor",NOW(),NOW());

INSERT INTO `users`(`rut_person`, `id_user_role`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES ("17746712", 1, "cgalvez", "camilogalvezv@gmail.com", "12345", "", NOW(),NOW());
INSERT INTO `users`(`rut_person`, `id_user_role`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES ("17746712", 2, "cgalvez", "camilogalvezv@gmail.com", "12345", "", NOW(),NOW());
INSERT INTO `users`(`rut_person`, `id_user_role`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES ("16889493", 3, "hzett", "halylzett@gmail.com", "12345", "", NOW(),NOW());
INSERT INTO `users`(`rut_person`, `id_user_role`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES ("18344216", 3, "ltorres", "ltorres12@alumnos.utalca.cl", "12345", "", NOW(),NOW());
INSERT INTO `users`(`rut_person`, `id_user_role`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES ("9100421", 3, "ogalvez", "cgalvez09@alumnos.utalca.cl", "12345", "", NOW(),NOW());
INSERT INTO `users`(`rut_person`, `id_user_role`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES ("7915421", 3, "mvidal", "motta_1512@hotmail.com", "12345", "", NOW(),NOW());

INSERT INTO `traps` (`id_client`, `id_trap_type`, `id_trap_location`, `trap_number`,`trap_letter`, `created_at`, `updated_at`) VALUES (1, 1, 1, 1, null, now(), now());
INSERT INTO `traps` (`id_client`, `id_trap_type`, `id_trap_location`, `trap_number`,`trap_letter`, `created_at`, `updated_at`) VALUES (1, 1, 2, 2, null, now(), now());
INSERT INTO `traps` (`id_client`, `id_trap_type`, `id_trap_location`, `trap_number`,`trap_letter`, `created_at`, `updated_at`) VALUES (1, 2, 2, 1, "A", now(), now());
INSERT INTO `traps` (`id_client`, `id_trap_type`, `id_trap_location`, `trap_number`,`trap_letter`, `created_at`, `updated_at`) VALUES (1, 2, 3, 1, "B", now(), now());
